﻿namespace DevInterview2.Services.SitemapGenerator
{
    public enum SitemapFrequency
    {
        Never,

        Yearly,

        Monthly,

        Weekly,

        Daily,

        Hourly,

        Always
    }
}
