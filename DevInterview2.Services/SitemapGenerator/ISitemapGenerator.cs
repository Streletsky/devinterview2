﻿using System;
using System.Collections.Generic;

namespace DevInterview2.Services.SitemapGenerator
{
    public interface ISitemapGenerator
    {
        #region Public Methods and Operators

        void AddSitemapNode(string url, double priority, SitemapFrequency freq, DateTime? lastModified = null);

        void AddSitemapNode(SitemapNode node);

        void AddSitemapNodes(IEnumerable<SitemapNode> nodes);

        string BuildSitemapDocument();

        #endregion
    }
}
