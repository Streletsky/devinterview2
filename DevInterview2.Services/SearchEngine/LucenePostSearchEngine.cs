﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using DevInterview2.Common.Exceptions;
using DevInterview2.Common.Exceptions.Business;
using DevInterview2.Data.Entities.Business;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers.Classic;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Util;
using Directory = Lucene.Net.Store.Directory;
using ParseException = System.Linq.Dynamic.Core.Exceptions.ParseException;

namespace DevInterview2.Services.SearchEngine
{
    public class LucenePostSearchEngine : IPostSearchEngine
    {
        #region Constants

        private const string BodyFieldName = "Body";

        private const string IdFieldName = "Id";

        private const string TagsFieldName = "Tags";

        private const string TitleFieldName = "Title";

        private const string CategoryFieldName = "Category";

        #endregion


        #region Static Fields

        private static FSDirectory DirectoryTemp;

        #endregion


        #region Fields

        private readonly StandardAnalyzer analyzer;

        private readonly string rootFolderPath;

        private readonly IndexWriter writer;

        #endregion


        #region Constructors and Destructors

        public LucenePostSearchEngine(string rootFolderPath)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                this.rootFolderPath = rootFolderPath + "/LuceneIndex";
            }
            else
            {
                this.rootFolderPath = rootFolderPath + "\\LuceneIndex";
            }

            analyzer = new StandardAnalyzer(LuceneVersion.LUCENE_48);
            writer = new IndexWriter(Directory, new IndexWriterConfig(LuceneVersion.LUCENE_48, analyzer));
        }

        #endregion


        #region Properties

        private Directory Directory
        {
            get
            {
                if (DirectoryTemp == null)
                {
                    var dir = new DirectoryInfo(rootFolderPath);
                    if (!dir.Exists)
                    {
                        dir = System.IO.Directory.CreateDirectory(rootFolderPath);
                    }

                    DirectoryTemp = FSDirectory.Open(dir, new SimpleFSLockFactory());
                }

                if (IndexWriter.IsLocked(DirectoryTemp))
                {
                    IndexWriter.Unlock(DirectoryTemp);
                }

                string lockFilePath = Path.Combine(rootFolderPath, "write.lock");
                if (File.Exists(lockFilePath))
                {
                    File.Delete(lockFilePath);
                }

                return DirectoryTemp;
            }
        }

        #endregion


        #region Public Methods and Operators

        public void AddPostsToIndex(IEnumerable<Post> posts)
        {
            foreach (Post post in posts)
            {
                AddPostToIndex(post);
            }

            writer.Commit();
            writer.Flush(true, true);
        }

        public void AddPostToIndex(Post post)
        {
            RemovePostFromIndex(post);

            var doc = new Document
            {
                new StringField(IdFieldName, post.Id.ToString(), Field.Store.YES),
                new TextField(BodyFieldName, post.BodyStart + post.BodyEnd, Field.Store.NO),
                new StringField(TitleFieldName, post.Title, Field.Store.YES),
                new StringField(CategoryFieldName, post.Category.Title, Field.Store.YES),
                new StringField(TagsFieldName,
                                string.Join(" ", post.TagPosts.Select(t => t.Tag.Title)),
                                Field.Store.YES)
            };

            writer.AddDocument(doc);
        }

        public void ClearIndex()
        {
            writer.DeleteAll();
            writer.Commit();
        }

        public void Dispose()
        {
            Directory.Dispose();
            analyzer.Dispose();
            writer.Dispose();
        }

        public void RemovePostFromIndex(Post post)
        {
            var searchQuery = new TermQuery(new Term(IdFieldName, post.Id.ToString()));
            writer.DeleteDocuments(searchQuery);
            writer.Commit();
        }

        public IEnumerable<int> Search(string searchQuery, int records)
        {
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", "")))
            {
                return new List<int>();
            }

            string[] fields = { BodyFieldName, CategoryFieldName, TitleFieldName, TagsFieldName };

            var boosts = new Dictionary<string, float>
            {
                { BodyFieldName, 25f },
                { CategoryFieldName, 35f },
                { TitleFieldName, 30f },
                { TagsFieldName, 10f }
            };

            var searcher = new IndexSearcher(writer.GetReader(true));
            var parser = new MultiFieldQueryParser(LuceneVersion.LUCENE_48, fields, analyzer, boosts)
            {
                DefaultOperator = Operator.OR
            };
            Query query = ParseQuery(searchQuery, parser);
            List<Document> results = searcher.Search(query, null, records, Sort.RELEVANCE).ScoreDocs
                                             .Select(hit => searcher.Doc(hit.Doc)).ToList();

            var ids = new List<int>();
            foreach (Document result in results)
            {
                if (!int.TryParse(result.Get("Id"), out int id))
                {
                    var inner = new ValidationException("Id of a Document in Lucene search index is corrupted.");

                    throw new BusinessLogicException(new ExceptionMessage
                                                     {
                                                         FailedOperationDescription =
                                                             $"Can not retrieve Post from Lucene search index by ID. Search Query: {searchQuery}",
                                                         TypeOfException = inner.GetType(),
                                                         PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                     },
                                                     inner);
                }

                ids.Add(id);
            }

            return ids;
        }

        public IEnumerable<int> Search(Post post, int records)
        {
            string queryForPost = post.Title + " " + string.Join(" ", post.TagPosts.Select(t => t.Tag.Title));

            return Search(queryForPost, records);
        }

        #endregion


        #region Methods

        private Query ParseQuery(string searchQuery, MultiFieldQueryParser parser)
        {
            List<string> terms = searchQuery.ToLowerInvariant().Replace("-", " ")
                                            .Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                                            .Select(x => QueryParserBase.Escape(x) + "~").ToList();
            var finalQuery = new BooleanQuery();

            try
            {
                foreach (string term in terms)
                {
                    finalQuery.Add(parser.Parse(term), Occur.SHOULD);
                }
            }
            catch (ParseException ex)
            {
                throw new BusinessLogicException(new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = "Can not perform Lucene search.",
                                                     TypeOfException = ex.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 ex);
            }

            return finalQuery;
        }

        #endregion
    }
}
