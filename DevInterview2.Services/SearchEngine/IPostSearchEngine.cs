﻿using System;
using System.Collections.Generic;
using DevInterview2.Data.Entities.Business;

namespace DevInterview2.Services.SearchEngine
{
    public interface IPostSearchEngine : IDisposable
    {
        #region Public Methods and Operators

        void AddPostsToIndex(IEnumerable<Post> post);

        void AddPostToIndex(Post post);

        void ClearIndex();

        void RemovePostFromIndex(Post post);

        IEnumerable<int> Search(string query, int records);

        IEnumerable<int> Search(Post post, int records);

        #endregion
    }
}
