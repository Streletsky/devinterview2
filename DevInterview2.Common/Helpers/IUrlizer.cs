﻿namespace DevInterview2.Common.Helpers
{
    public interface IUrlizer
    {
        #region Public Methods and Operators

        string GetFriendlyUrl(string str);

        #endregion
    }
}