﻿namespace DevInterview2.Common.Helpers
{
    public interface IBotDetector
    {
        #region Public Methods and Operators

        bool CheckUserAgentForBot(string ua);

        #endregion
    }
}