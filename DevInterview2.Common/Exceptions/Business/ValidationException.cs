﻿using System;
using System.Runtime.Serialization;

namespace DevInterview2.Common.Exceptions.Business
{
    [Serializable]
    public class ValidationException : Exception
    {
        #region Constructors and Destructors

        public ValidationException() { }

        public ValidationException(string message) : base(message) { }

        public ValidationException(string message, Exception inner) : base(message, inner) { }

        protected ValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion
    }
}