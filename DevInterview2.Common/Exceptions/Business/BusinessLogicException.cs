﻿using System;
using System.Runtime.Serialization;

namespace DevInterview2.Common.Exceptions.Business
{
    [Serializable]
    public class BusinessLogicException : Exception
    {
        #region Constructors and Destructors

        public BusinessLogicException() { }

        public BusinessLogicException(string message) : base(message) { }

        public BusinessLogicException(string message, Exception inner) : base(message, inner) { }

        public BusinessLogicException(ExceptionMessage message, Exception inner) :
            base(ExceptionHelper.GetErrorMessage(message), inner) { }

        protected BusinessLogicException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion
    }
}