﻿using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Base;

namespace DevInterview2.Data.Repositories.Business.Interfaces
{
    public interface IPostRepository : IRepository<Post, int>
    {
        #region Public Methods and Operators

        IQueryable<Post> GetAllAvailable(bool showInvisible = false);

        IQueryable<Post> GetByCategory(int categoryId);

        IQueryable<Post> GetByTag(int tagId);

        #endregion
    }
}
