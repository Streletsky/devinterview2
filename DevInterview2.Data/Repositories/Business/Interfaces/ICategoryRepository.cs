﻿using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Base;

namespace DevInterview2.Data.Repositories.Business.Interfaces
{
    public interface ICategoryRepository : IRepository<Category, int>
    { }
}
