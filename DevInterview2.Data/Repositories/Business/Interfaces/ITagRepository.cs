﻿using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Base;

namespace DevInterview2.Data.Repositories.Business.Interfaces
{
    public interface ITagRepository : IRepository<Tag, int>
    {
        #region Public Methods and Operators

        IQueryable<Tag> GetAllAvailable(bool showInvisible = false);

        #endregion
    }
}
