﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Common.Exceptions;
using DevInterview2.Common.Exceptions.Business;
using DevInterview2.Common.Extensions;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.OrmInfrastructure;
using DevInterview2.Data.Repositories.Base;
using DevInterview2.Data.Repositories.Business.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DevInterview2.Data.Repositories.Business
{
    public class PostRepository : EfRepository<Post, int>, IPostRepository
    {
        #region Constructors and Destructors

        public PostRepository(AppDbContext context) : base(context) { }

        #endregion


        #region Public Methods and Operators

        public IQueryable<Post> GetAllAvailable(bool showInvisible = false) =>
            IncludeEntities().Where(p => !p.IsDeleted && (p.IsVisible || showInvisible));

        public IQueryable<Post> GetByCategory(int categoryId) =>
            IncludeEntities().Where(p => p.CategoryId == categoryId && !p.IsDeleted && p.IsVisible);

        public IQueryable<Post> GetByTag(int tagId) => 
            IncludeEntities().Where(p => p.TagPosts.Any(t => t.TagId == tagId) && !p.IsDeleted && p.IsVisible);

        public override Task<Post> GetAsync(int id) => IncludeEntities().SingleOrDefaultAsync(p => p.Id == id);

        public override async Task<Post> InsertAsync(Post entity)
        {
            entity.Created = DateTime.Now;

            PreparePostForSaving(entity);

            return await base.InsertAsync(entity);
        }

        public override async Task<Post> UpdateAsync(Post entity)
        {
            PreparePostForSaving(entity);

            return await base.UpdateAsync(entity);
        }

        #endregion


        #region Methods

        private IQueryable<Post> IncludeEntities()
        {
            return EntitiesSet.Include(p => p.Category)
                              .Include(p => p.TagPosts)
                              .ThenInclude(p => p.Tag)
                              .Include(p => p.Rating).AsNoTracking();
        }

        private void PreparePostForSaving(Post entity)
        {
            ValidatePost(entity);
            ClearHtml(entity);
            
            entity.LastUpdated = DateTime.Now;
        }
        
        private void ClearHtml(Post post)
        {
            post.BodyStart = post.BodyStart.Replace("&nbsp;", " ");

            if (post.BodyEnd != null)
            {
                post.BodyEnd = post.BodyEnd.Replace("&nbsp;", " ");
            }
        }
        
        private void ValidatePost(Post entity)
        {
            var inner = new ValidationException();
            var exThrown = false;

            if (entity.Category == null)
            {
                inner = new ValidationException("Category of a Post can not be null.");
                exThrown = true;
            }
            else if (entity.Category.Id == 0)
            {
                inner = new ValidationException("ID of Category of a Post can not be 0.");
                exThrown = true;
            }

            if (entity.BodyStart.IsNullOrEmpty())
            {
                inner = new ValidationException("BodyStart of a Post can not be null or empty.");
                exThrown = true;
            }

            if (entity.Title.IsNullOrEmpty())
            {
                inner = new ValidationException("Title of a Post can not be null or empty.");
                exThrown = true;
            }

            if (entity.UrlSlug.IsNullOrEmpty())
            {
                inner = new ValidationException("UrlSlug of a Post can not be null or empty.");
                exThrown = true;
            }

            if (exThrown)
            {
                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Can not create or update entry of type {entity.GetType().Name}.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 inner);
            }
        }

        #endregion
    }
}
