﻿using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.OrmInfrastructure;
using DevInterview2.Data.Repositories.Base;
using DevInterview2.Data.Repositories.Business.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DevInterview2.Data.Repositories.Business
{
    public class TagRepository : EfRepository<Tag, int>, ITagRepository
    {
        #region Constructors and Destructors

        public TagRepository(AppDbContext context) : base(context) { }

        #endregion


        #region Public Methods and Operators

        public IQueryable<Tag> GetAllAvailable(bool showInvisible = false) =>
            EntitiesSet.Include(t => t.TagPosts)
                       .ThenInclude(tp => tp.Post)
                       .Where(t => t.TagPosts.Any(tp => !tp.Post.IsDeleted && (tp.Post.IsVisible || showInvisible)));

        #endregion
    }
}
