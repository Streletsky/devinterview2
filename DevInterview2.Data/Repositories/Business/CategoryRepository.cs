﻿using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.OrmInfrastructure;
using DevInterview2.Data.Repositories.Base;
using DevInterview2.Data.Repositories.Business.Interfaces;

namespace DevInterview2.Data.Repositories.Business
{
    public class CategoryRepository : EfRepository<Category, int>, ICategoryRepository
    {
        #region Constructors and Destructors

        public CategoryRepository(AppDbContext context) : base(context) { }

        #endregion
    }
}
