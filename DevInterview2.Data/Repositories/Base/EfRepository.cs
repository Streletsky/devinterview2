﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DevInterview2.Data.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace DevInterview2.Data.Repositories.Base
{
    /// <summary>
    ///     Serves as base class for EF based repositories with CRUD operations implemented.
    /// </summary>
    /// <typeparam name="TEntity">
    ///     Type of entity repository will work with.
    /// </typeparam>
    /// <typeparam name="TId">
    ///     Type of ID of <typeparamref name="TEntity" />.
    /// </typeparam>
    public abstract class EfRepository<TEntity, TId> : IRepository<TEntity, TId> where TEntity : class, IEntity<TId>
    {
        #region Fields

        private readonly DbContext context;

        #endregion


        #region Constructors and Destructors

        protected EfRepository(DbContext context)
        {
            this.context = context;
            EntitiesSet = this.context.Set<TEntity>();
        }

        #endregion


        #region Properties

        protected DbSet<TEntity> EntitiesSet { get; set; }

        #endregion


        #region Public Methods and Operators

        public virtual async Task DeleteAsync(TId id)
        {
            TEntity entity = await GetAsync(id);

            await DeleteAsync(entity);
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            EntitiesSet.Remove(entity);

            await context.SaveChangesAsync();
        }

        public virtual IQueryable<TEntity> GetAll() => EntitiesSet;

        public virtual Task<TEntity> GetAsync(TId id) => EntitiesSet.FindAsync(id).AsTask();

        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            EntitiesSet.Add(entity);

            await context.SaveChangesAsync();

            return entity;
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            EntitiesSet.Update(entity);

            await context.SaveChangesAsync();

            return entity;
        }

        #endregion
    }
}
