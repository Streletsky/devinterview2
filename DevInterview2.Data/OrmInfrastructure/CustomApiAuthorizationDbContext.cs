﻿using System;
using System.Threading.Tasks;
using Duende.IdentityServer.EntityFramework.Entities;
using Duende.IdentityServer.EntityFramework.Extensions;
using Duende.IdentityServer.EntityFramework.Interfaces;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace DevInterview2.Data.OrmInfrastructure
{
    public class
        CustomApiAuthorizationDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken> :
            IdentityDbContext<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken>,
            IPersistedGrantDbContext where TUser : IdentityUser<TKey>
                                     where TRole : IdentityRole<TKey>
                                     where TKey : IEquatable<TKey>
                                     where TUserClaim : IdentityUserClaim<TKey>
                                     where TUserRole : IdentityUserRole<TKey>
                                     where TUserLogin : IdentityUserLogin<TKey>
                                     where TRoleClaim : IdentityRoleClaim<TKey>
                                     where TUserToken : IdentityUserToken<TKey>
    {
        #region Fields

        private readonly IOptions<OperationalStoreOptions> operationalStoreOptions;

        #endregion


        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of <see cref="ApiAuthorizationDbContext{TUser}" />.
        /// </summary>
        /// <param name="options">The <see cref="DbContextOptions" />.</param>
        /// <param name="operationalStoreOptions">The <see cref="IOptions{OperationalStoreOptions}" />.</param>
        public CustomApiAuthorizationDbContext(DbContextOptions options,
                                               IOptions<OperationalStoreOptions> operationalStoreOptions) :
            base(options)
        {
            this.operationalStoreOptions = operationalStoreOptions;
        }

        #endregion


        #region Public Properties

        /// <summary>
        ///     Gets or sets the <see cref="DbSet{DeviceFlowCodes}" />.
        /// </summary>
        public DbSet<DeviceFlowCodes> DeviceFlowCodes { get; set; }

        public DbSet<Key> Keys { get; set; }

        /// <summary>
        ///     Gets or sets the <see cref="DbSet{PersistedGrant}" />.
        /// </summary>
        public DbSet<PersistedGrant> PersistedGrants { get; set; }

        #endregion


        #region Explicit Interface Methods

        Task<int> IPersistedGrantDbContext.SaveChangesAsync() => base.SaveChangesAsync();

        #endregion


        #region Methods

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ConfigurePersistedGrantContext(operationalStoreOptions.Value);
        }

        #endregion
    }
}
