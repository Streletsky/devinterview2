﻿using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Entities.Identity;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace DevInterview2.Data.OrmInfrastructure
{
    public class AppDbContext : CustomApiAuthorizationDbContext<AppUser, AppRole, int, AppUserClaim, AppUserRole,
        AppUserLogin, AppRoleClaim, AppUserToken>
    {
        #region Constructors and Destructors

        public AppDbContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions) :
            base(options, operationalStoreOptions) { }

        #endregion


        #region Public Properties

        public DbSet<Category> Categories { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Rating> Ratings { get; set; }

        public DbSet<Tag> Tags { get; set; }

        #endregion


        #region Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TagPosts>().HasKey(sc => new
            {
                sc.PostId,
                sc.TagId
            });
        }

        #endregion
    }
}
