﻿using Microsoft.AspNetCore.Identity;

namespace DevInterview2.Data.Entities.Identity
{
    public class AppUserRole : IdentityUserRole<int> { }
}