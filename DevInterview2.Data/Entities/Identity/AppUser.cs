﻿using System;
using Microsoft.AspNetCore.Identity;

namespace DevInterview2.Data.Entities.Identity
{
    public class AppUser : IdentityUser<int>
    {
        #region Public Properties

        public DateTime Created { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        #endregion
    }
}
