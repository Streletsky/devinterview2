﻿using Microsoft.AspNetCore.Identity;

namespace DevInterview2.Data.Entities.Identity
{
    public class AppUserLogin : IdentityUserLogin<int> { }
}