﻿using DevInterview2.Data.Entities.Base;

namespace DevInterview2.Data.Entities.Business.Base
{
    /// <summary>
    ///     Serves as base class for entities, which have own URL in application and title.
    /// </summary>
    public abstract class WebEntry : BaseEntity
    {
        #region Public Properties

        public string Title { get; set; }

        public string UrlSlug { get; set; }

        #endregion
    }
}