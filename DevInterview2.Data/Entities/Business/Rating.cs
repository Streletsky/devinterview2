﻿using System.ComponentModel.DataAnnotations.Schema;
using DevInterview2.Data.Entities.Base;

namespace DevInterview2.Data.Entities.Business
{
    public class Rating : BaseEntity
    {
        #region Public Properties

        [NotMapped] public int VotesDifference => VotesUp - VotesDown;

        public int VotesDown { get; set; }

        public int VotesUp { get; set; }

        #endregion
    }
}