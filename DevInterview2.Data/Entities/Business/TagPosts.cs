﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DevInterview2.Data.Entities.Business
{
    public class TagPosts
    {
        #region Public Properties

        [ForeignKey("PostId")] public Post Post { get; set; }

        [Column("Post_Id")] public int PostId { get; set; }

        [ForeignKey("TagId")] public Tag Tag { get; set; }

        [Column("Tag_Id")] public int TagId { get; set; }

        #endregion
    }
}