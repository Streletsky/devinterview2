﻿using System.Collections.Generic;
using DevInterview2.Data.Entities.Business.Base;

namespace DevInterview2.Data.Entities.Business
{
    public class Tag : WebEntry
    {
        #region Public Properties

        public ICollection<TagPosts> TagPosts { get; set; }

        #endregion
    }
}