﻿using System.Collections.Generic;
using DevInterview2.Data.Entities.Business.Base;

namespace DevInterview2.Data.Entities.Business
{
    public class Category : WebEntry
    {
        #region Public Properties

        public string Description { get; set; }

        public ICollection<Post> Posts { get; set; } = new List<Post>();

        #endregion
    }
}
