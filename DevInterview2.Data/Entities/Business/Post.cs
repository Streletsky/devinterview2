﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using DevInterview2.Data.Entities.Business.Base;
using DevInterview2.Data.Entities.Identity;

namespace DevInterview2.Data.Entities.Business
{
    public class Post : WebEntry
    {
        #region Public Properties

        public string BodyEnd { get; set; }

        public string BodyStart { get; set; }

        [ForeignKey("CategoryId")] public Category Category { get; set; }

        [Column("Category_Id")] public int? CategoryId { get; set; }

        public DateTime Created { get; set; }

        [ForeignKey("CreatedById")] public AppUser CreatedBy { get; set; }

        [Column("CreatedBy_Id")] public int? CreatedById { get; set; }

        public string Description { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsLinkingRequired { get; set; }

        public bool IsVisible { get; set; }

        public DateTime LastUpdated { get; set; }

        [ForeignKey("LastUpdatedById")] public AppUser LastUpdatedBy { get; set; }

        [Column("LastUpdatedBy_Id")] public int? LastUpdatedById { get; set; }

        [ForeignKey("RatingId")] public Rating Rating { get; set; }

        [Column("Rating_Id")] public int? RatingId { get; set; }

        public ICollection<TagPosts> TagPosts { get; set; } = new List<TagPosts>();

        public int Views { get; set; }

        #endregion
    }
}