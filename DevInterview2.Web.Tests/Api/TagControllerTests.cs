﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business;
using DevInterview2.Web.Controllers;
using DevInterview2.Web.DataTransferObjects;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace DevInterview2.Web.Tests.Api
{
    [TestFixture]
    public class TagControllerTests : ApiTestBase<TagController>
    {
        #region Setup/Teardown Methods

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            var loggerMock = new Mock<ILogger<TagController>>();

            tagRepositoryMock = new Mock<TagRepository>(Context)
            {
                CallBase = true
            };

            postRepositoryMock = new Mock<PostRepository>(Context)
            {
                CallBase = true
            };

            Target = new TagController(loggerMock.Object, ConfiguredMapper, tagRepositoryMock.Object, postRepositoryMock.Object);
        }

        #endregion


        #region All other members

        private Mock<TagRepository> tagRepositoryMock;

        private Mock<PostRepository> postRepositoryMock;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public async Task GetPostsByTag_WithInvalidId_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.GetPostsByTag(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(1)]
        public async Task GetPostsByTag_WithPostsInStorage_ShouldNotReturnDeletedPosts(int id)
        {
            Post deletedPost =
                await Context.Posts.FirstOrDefaultAsync(p => p.IsDeleted && p.TagPosts.Any(t => t.TagId == id));

            SearchResult<PostDto> result = (await Target.GetPostsByTag(id)).Value;

            result.Data.Any(p => p.Id == deletedPost.Id).Should().BeFalse();
        }

        [Test]
        [TestCase(1)]
        public async Task GetPostsByTag_WithPostsInStorage_ShouldNotReturnNotVisiblePosts(int id)
        {
            Post invisiblePost =
                await Context.Posts.FirstOrDefaultAsync(p => !p.IsVisible && p.TagPosts.Any(t => t.TagId == id));

            SearchResult<PostDto> result = (await Target.GetPostsByTag(id)).Value;

            result.Data.Any(p => p.Id == invisiblePost.Id).Should().BeFalse();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByTag_WithPostsInStorage_ShouldReturnAllAvailablePostsInTag(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByTag(id)).Value;

            result.TotalCount.Should().Be(1);
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByTag_WithPostsInStorage_ShouldReturnPostDtoSearchResult(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByTag(id)).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<SearchResult<PostDto>>();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByTag_WithPostsInStorage_ShouldReturnResultOrderedByCreated(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByTag(id)).Value;

            Assert.That(result.Data, Is.Ordered.By("Created").Descending);
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByTag_WithTagInStorageAndNoPosts_ShouldReturnNotFound(int id)
        {
            List<Post> posts = await Context.Posts.Where(p => p.TagPosts.Any(t => t.TagId == id)).ToListAsync();
            foreach (Post post in posts)
            {
                Context.Posts.Remove(post);
            }

            await Context.SaveChangesAsync();

            ActionResult result = (await Target.GetPostsByTag(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(999)]
        public async Task GetPostsByTag_WithTagNotFoundInStorage_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.GetPostsByTag(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithPostsInStorage_ShouldRemoveBodyFromPosts(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByTag(id)).Value;

            result.Data.First().BodyEnd.Should().BeNullOrEmpty();
        }

        [Test]
        public async Task GetTags_WithTagsInStorage_ShouldReturnResultOrderedByPostsCountDesc()
        {
            TagDto[] result = (await Target.GetTags()).Value;

            Assert.That(result, Is.Ordered.By("PostsCount").Descending);
        }

        [Test]
        public async Task GetTags_WithTagsInStorage_ShouldReturnTagDtoArray()
        {
            TagDto[] result = (await Target.GetTags()).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<TagDto[]>();
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetTags_WithTagsInStorage_ShouldReturnWithPostsCount(int id)
        {
            int postsByTagCount = await Context.Posts.Where(p => p.TagPosts.Any(t => t.TagId == id)).CountAsync();

            TagDto[] result = (await Target.GetTags()).Value;

            TagDto tag = result.First(c => c.Id == id);
            tag.PostsCount.Should().Be(postsByTagCount);
        }

        [Test]
        public async Task GetTags_WithZeroPostsForTag_ShouldNotReturnTag()
        {
            var emptyTag = new Tag
            {
                Title = "empty",
                UrlSlug = "empty"
            };
            await Context.Tags.AddAsync(emptyTag);
            await Context.SaveChangesAsync();

            TagDto[] result = (await Target.GetTags()).Value;

            result.Any(c => c.Id == emptyTag.Id).Should().BeFalse();
        }

        [Test]
        public async Task GetTags_WithZeroTags_ShouldReturnNotFound()
        {
            List<Tag> cats = await Context.Tags.ToListAsync();
            foreach (Tag tag in cats)
            {
                Context.Tags.Remove(tag);
            }

            await Context.SaveChangesAsync();

            ActionResult result = (await Target.GetTags()).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        #endregion
    }
}
