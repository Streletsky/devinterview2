﻿using System;
using AutoMapper;
using DevInterview2.Data.OrmInfrastructure;
using DevInterview2.Web.DataTransferObjects;
using DevInterview2.Web.Tests.TestHelpers;
using Duende.IdentityServer.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace DevInterview2.Web.Tests.Api
{
    public class ApiTestBase<T>
    {
        #region Setup/Teardown Methods

        [SetUp]
        public virtual void Setup()
        {
            var storeOptionsMock = new Mock<IOptions<OperationalStoreOptions>>();
            var storeOptionsValueMock = new Mock<OperationalStoreOptions>();
            storeOptionsMock.SetupGet(o => o.Value).Returns(storeOptionsValueMock.Object);
            DbContextOptions<AppDbContext> options = new DbContextOptionsBuilder<AppDbContext>()
                                                     .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            Context = new AppDbContext(options, storeOptionsMock.Object);
            Context.AddRange(FakeDataProvider.PopulateRatingStore());
            Context.AddRange(FakeDataProvider.PopulateTagStore());
            Context.AddRange(FakeDataProvider.PopulateCategoryStore());
            Context.AddRange(FakeDataProvider.PopulatePostStore());
            Context.SaveChanges();
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new PostDtoProfile());
                cfg.AddProfile(new CategoryDtoProfile());
                cfg.AddProfile(new TagDtoProfile());
                cfg.AddProfile(new RatingDtoProfile());
            });

            ConfiguredMapper = mapper.CreateMapper();
        }

        [TearDown]
        public void TearDown()
        {
            Context.Dispose();
        }

        #endregion


        #region All other members

        protected IMapper ConfiguredMapper;

        protected T Target;

        protected AppDbContext Context;

        #endregion
    }
}
