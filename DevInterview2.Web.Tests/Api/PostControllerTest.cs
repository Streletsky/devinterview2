﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Common.Exceptions.Business;
using DevInterview2.Common.Helpers;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business;
using DevInterview2.Services.SearchEngine;
using DevInterview2.Web.Controllers;
using DevInterview2.Web.DataTransferObjects;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace DevInterview2.Web.Tests.Api
{
    [TestFixture]
    public class PostControllerTest : ApiTestBase<PostController>
    {
        #region Setup/Teardown Methods

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            luceneSearchEngineMock = new Mock<IPostSearchEngine>();
            luceneSearchEngineMock.Setup(e => e.Search("title", It.IsAny<int>()))
                                  .Returns(new List<int>
                                  {
                                      1
                                  });
            luceneSearchEngineMock.Setup(e => e.Search("notfound", It.IsAny<int>())).Returns(new List<int>());
            luceneSearchEngineMock.Setup(e => e.Search(It.IsAny<Post>(), It.IsAny<int>()))
                                  .Returns(new List<int>
                                  {
                                      1,
                                      2,
                                      3,
                                      4,
                                      5
                                  });

            botDetectorMock = new Mock<IBotDetector>();
            botDetectorMock.Setup(e => e.CheckUserAgentForBot(It.IsAny<string>())).Returns(false);

            var loggerMock = new Mock<ILogger<PostController>>();

            postRepositoryMock = new Mock<PostRepository>(Context)
            {
                CallBase = true
            };

            tagRepositoryMock = new Mock<TagRepository>(Context)
            {
                CallBase = true
            };
            
            categoryRepositoryMock = new Mock<CategoryRepository>(Context)
            {
                CallBase = true
            };
            
            urlizerMock = new Mock<IUrlizer>();

            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers["User-Agent"] = "fake_ua";

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            Target = new PostController(loggerMock.Object,
                                        postRepositoryMock.Object,
                                        botDetectorMock.Object,
                                        ConfiguredMapper,
                                        luceneSearchEngineMock.Object,
                                        tagRepositoryMock.Object,
                                        categoryRepositoryMock.Object,
                                        urlizerMock.Object)
            {
                ControllerContext = controllerContext
            };
        }

        #endregion


        #region All other members

        private Mock<IBotDetector> botDetectorMock;

        private Mock<PostRepository> postRepositoryMock;

        private Mock<TagRepository> tagRepositoryMock;
        
        private Mock<CategoryRepository> categoryRepositoryMock;

        private Mock<IPostSearchEngine> luceneSearchEngineMock;

        private Mock<IUrlizer> urlizerMock;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public async Task Get_WithInvalidId_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.Get(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(1)]
        public async Task Get_WithPostFoundInStorage_ShouldCheckForBot(int id)
        {
            await Target.Get(id);

            botDetectorMock.Verify(e => e.CheckUserAgentForBot(It.IsAny<string>()), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public async Task Get_WithPostFoundInStorage_ShouldIncreaseViews(int id)
        {
            Post postInStorage = await Context.Posts.FindAsync(id);
            int viewsBefore = postInStorage.Views;

            await Target.Get(id);

            postInStorage = await Context.Posts.FindAsync(id);
            int viewsAfter = postInStorage.Views;
            viewsAfter.Should().Be(viewsBefore + 1);
        }

        [Test]
        [TestCase(1)]
        public async Task Get_WithPostFoundInStorage_ShouldReturnPostDto(int id)
        {
            Post postInStorage = await Context.Posts.FindAsync(id);
            PostDto result = (await Target.Get(id)).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<PostDto>();
            result.Title.Should().Be(postInStorage.Title);
        }

        [Test]
        [TestCase(1)]
        public async Task Get_WithPostFoundInStorageRequestedByBot_ShouldNotIncreaseViews(int id)
        {
            Post postInStorage = await Context.Posts.FindAsync(id);
            int viewsBefore = postInStorage.Views;
            botDetectorMock.Setup(e => e.CheckUserAgentForBot(It.IsAny<string>())).Returns(true);

            await Target.Get(id);

            postInStorage = await Context.Posts.FindAsync(id);
            int viewsAfter = postInStorage.Views;
            viewsBefore.Should().Be(viewsAfter);
        }

        [Test]
        [TestCase(3)]
        public async Task Get_WithPostMarkedDeleted_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.Get(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(4)]
        public async Task Get_WithPostMarkedNotVisible_ShouldReturnPostDto(int id)
        {
            Post postInStorage = await Context.Posts.FindAsync(id);
            PostDto result = (await Target.Get(id)).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<PostDto>();
            result.Title.Should().Be(postInStorage.Title);
        }

        [Test]
        [TestCase(999)]
        public async Task Get_WithPostNotFoundInStorage_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.Get(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase("None")]
        public void GetPosts_WithInvalidSortColumn_ThrowsValidationException(string sortColumn)
        {
            Assert.ThrowsAsync<ValidationException>(async () => await Target.GetPosts(sortColumn: sortColumn));
        }

        [Test]
        [TestCase("title")]
        public async Task GetPosts_WithPostFoundBySearchQuery_ShouldReturnPostFromIndex(string searchQuery)
        {
            Post postInStorage = await Context.Posts.FindAsync(1);

            SearchResult<PostDto> result = (await Target.GetPosts(searchQuery: searchQuery)).Value;

            result.TotalCount.Should().BeGreaterOrEqualTo(1);
            result.Data.Any(p => p.Id == postInStorage.Id).Should().BeTrue();
        }

        [Test]
        [TestCase("notfound")]
        public async Task GetPosts_WithPostNotFoundBySearchQuery_ShouldReturnNotFound(string searchQuery)
        {
            ActionResult result = (await Target.GetPosts(searchQuery: searchQuery)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task GetPosts_WithPostsInStorage_ShouldNotReturnDeletedPosts()
        {
            Post deletedPost = await Context.Posts.FirstOrDefaultAsync(p => p.IsDeleted);

            SearchResult<PostDto> result = (await Target.GetPosts()).Value;

            result.Data.Any(p => p.Id == deletedPost.Id).Should().BeFalse();
        }

        [Test]
        public async Task GetPosts_WithPostsInStorage_ShouldNotReturnNotVisiblePosts()
        {
            Post invisiblePost = await Context.Posts.FirstOrDefaultAsync(p => !p.IsVisible);

            SearchResult<PostDto> result = (await Target.GetPosts()).Value;

            result.Data.Any(p => p.Id == invisiblePost.Id).Should().BeFalse();
        }

        [Test]
        public async Task GetPosts_WithPostsInStorage_ShouldRemoveBodyFromPosts()
        {
            SearchResult<PostDto> result = (await Target.GetPosts(showInvisible: true)).Value;

            result.Data.First().BodyEnd.Should().BeNullOrEmpty();
        }

        [Test]
        public async Task GetPosts_WithPostsInStorage_ShouldReturnPostDtoSearchResult()
        {
            SearchResult<PostDto> result = (await Target.GetPosts()).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<SearchResult<PostDto>>();
        }

        [Test]
        public async Task GetPosts_WithPostsInStorageAndInvisibleParam_ShouldReturnNotVisiblePosts()
        {
            Post invisiblePost = await Context.Posts.FirstOrDefaultAsync(p => !p.IsVisible);

            SearchResult<PostDto> result = (await Target.GetPosts(showInvisible: true)).Value;

            result.Data.Any(p => p.Id == invisiblePost.Id).Should().BeTrue();
        }

        [Test]
        public async Task GetPosts_WithPostsInStorageAndNoParams_ShouldReturnResultOrderedByCreated()
        {
            SearchResult<PostDto> result = (await Target.GetPosts()).Value;

            Assert.That(result.Data, Is.Ordered.By("Created").Descending);
        }

        [Test]
        [TestCase("Rating")]
        public async Task GetPosts_WithPostsInStorageAndSortByRating_ShouldReturnResultOrderedByRating(
            string sortColumn)
        {
            SearchResult<PostDto> result = (await Target.GetPosts(sortColumn: sortColumn)).Value;

            Assert.That(result.Data.Select(d => d.Rating), Is.Ordered.By("VotesDifference").Descending);
        }

        [Test]
        [TestCase("Views")]
        public async Task GetPosts_WithPostsInStorageAndSortColumnParam_ShouldReturnResultOrderedByParam(
            string sortColumn)
        {
            SearchResult<PostDto> result = (await Target.GetPosts(sortColumn: sortColumn)).Value;

            Assert.That(result.Data, Is.Ordered.By("Views").Descending);
        }

        [Test]
        [TestCase("title")]
        public async Task GetPosts_WithSearchQuery_ShouldUseLuceneSearchEngine(string searchQuery)
        {
            await Target.GetPosts(searchQuery: searchQuery);

            luceneSearchEngineMock.Verify(e => e.Search(searchQuery, 10), Times.Once);
        }

        [Test]
        public async Task GetPosts_WithZeroPosts_ShouldReturnNotFound()
        {
            List<Post> posts = await Context.Posts.ToListAsync();
            foreach (Post post in posts)
            {
                Context.Posts.Remove(post);
            }

            await Context.SaveChangesAsync();

            ActionResult result = (await Target.GetPosts()).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public async Task GetSimilarPosts_WithInvalidId_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.GetSimilarPosts(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(1)]
        public async Task GetSimilarPosts_WithPostFoundInStorage_ShouldNotReturnDeletedPosts(int id)
        {
            Post deletedPost = await Context.Posts.FirstOrDefaultAsync(p => p.IsDeleted);

            SearchResult<PostDto> result = (await Target.GetSimilarPosts(id)).Value;

            result.Data.Any(p => p.Id == deletedPost.Id).Should().BeFalse();
        }

        [Test]
        [TestCase(1)]
        public async Task GetSimilarPosts_WithPostFoundInStorage_ShouldNotReturnNotVisiblePosts(int id)
        {
            Post invisiblePost = await Context.Posts.FirstOrDefaultAsync(p => !p.IsVisible);

            SearchResult<PostDto> result = (await Target.GetSimilarPosts(id)).Value;

            result.Data.Any(p => p.Id == invisiblePost.Id).Should().BeFalse();
        }

        [Test]
        [TestCase(1)]
        public async Task GetSimilarPosts_WithPostFoundInStorage_ShouldNotReturnSelf(int id)
        {
            SearchResult<PostDto> result = (await Target.GetSimilarPosts(id)).Value;

            result.Data.Any(p => p.Id == id).Should().BeFalse();
        }

        [Test]
        [TestCase(1)]
        public async Task GetSimilarPosts_WithPostFoundInStorage_ShouldRemoveBodyFromPosts(int id)
        {
            SearchResult<PostDto> result = (await Target.GetSimilarPosts(id)).Value;

            result.Data.First().BodyStart.Should().BeNullOrEmpty();
            result.Data.First().BodyEnd.Should().BeNullOrEmpty();
        }

        [Test]
        [TestCase(1)]
        public async Task GetSimilarPosts_WithPostFoundInStorage_ShouldReturnPostDtoSearchResult(int id)
        {
            SearchResult<PostDto> result = (await Target.GetSimilarPosts(id)).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<SearchResult<PostDto>>();
        }

        [Test]
        [TestCase(1)]
        public async Task GetSimilarPosts_WithPostFoundInStorage_ShouldUseLuceneSearchEngine(int id)
        {
            Post postInStorage = await Context.Posts.FindAsync(id);

            await Target.GetSimilarPosts(id);

            luceneSearchEngineMock.Verify(e => e.Search(postInStorage, 6), Times.Once);
        }

        [Test]
        [TestCase(999)]
        public async Task GetSimilarPosts_WithPostNotFoundInStorage_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.GetSimilarPosts(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(1)]
        public async Task GetSimilarPosts_WithZeroSimilarPosts_ShouldReturnNotFound(int id)
        {
            luceneSearchEngineMock.Setup(e => e.Search(It.IsAny<Post>(), It.IsAny<int>())).Returns(new List<int>());

            ActionResult result = (await Target.GetSimilarPosts(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(1)]
        public async Task Vote_WithPostFoundInStorageAndVoteDown_ShouldVoteDown(int id)
        {
            Post postInStorage = await Context.Posts.FindAsync(id);
            int votesDownBefore = postInStorage.Rating.VotesDown;

            ActionResult result = await Target.Vote(id, false);

            postInStorage = await Context.Posts.FindAsync(id);
            int votesDownAfter = postInStorage.Rating.VotesDown;

            votesDownAfter.Should().Be(votesDownBefore + 1);
        }

        [Test]
        [TestCase(1)]
        public async Task Vote_WithPostFoundInStorageAndVoteUp_ShouldVoteUp(int id)
        {
            Post postInStorage = await Context.Posts.FindAsync(id);
            int voteUpsBefore = postInStorage.Rating.VotesUp;

            ActionResult result = await Target.Vote(id, true);

            postInStorage = await Context.Posts.FindAsync(id);
            int voteUpsAfter = postInStorage.Rating.VotesUp;

            voteUpsAfter.Should().Be(voteUpsBefore + 1);
        }

        #endregion
    }
}
