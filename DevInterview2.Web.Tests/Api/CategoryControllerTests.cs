﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Common.Helpers;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business;
using DevInterview2.Web.Controllers;
using DevInterview2.Web.DataTransferObjects;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace DevInterview2.Web.Tests.Api
{
    [TestFixture]
    public class CategoryControllerTests : ApiTestBase<CategoryController>
    {
        #region Setup/Teardown Methods

        [SetUp]
        public override void Setup()
        {
            base.Setup();

            var loggerMock = new Mock<ILogger<CategoryController>>();

            categoryRepositoryMock = new Mock<CategoryRepository>(Context)
            {
                CallBase = true
            };

            postRepositoryMock = new Mock<PostRepository>(Context)
            {
                CallBase = true
            };
            
            urlizerMock = new Mock<IUrlizer>();

            Target = new CategoryController(loggerMock.Object, ConfiguredMapper, categoryRepositoryMock.Object, postRepositoryMock.Object, urlizerMock.Object);
        }

        #endregion


        #region All other members

        private Mock<CategoryRepository> categoryRepositoryMock;

        private Mock<PostRepository> postRepositoryMock;

        private Mock<IUrlizer> urlizerMock;

        #endregion


        #region Test Methods

        [Test]
        public async Task GetCategories_WithCategoriesInStorage_ShouldReturnCategoryDtoArray()
        {
            CategoryDto[] result = (await Target.GetCategories()).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<CategoryDto[]>();
        }

        [Test]
        public async Task GetCategories_WithCategoriesInStorage_ShouldReturnResultOrderedByTitle()
        {
            CategoryDto[] result = (await Target.GetCategories()).Value;

            Assert.That(result, Is.Ordered.By("Title").Ascending);
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetCategories_WithCategoriesInStorage_ShouldReturnWithPostsCount(int id)
        {
            int postsByCategoryCount = await Context.Posts.Where(p => p.CategoryId == id).CountAsync();

            CategoryDto[] result = (await Target.GetCategories()).Value;

            CategoryDto category = result.First(c => c.Id == id);
            category.PostsCount.Should().Be(postsByCategoryCount);
        }

        [Test]
        public async Task GetCategories_WithZeroCategories_ShouldReturnNotFound()
        {
            List<Category> cats = await Context.Categories.ToListAsync();
            foreach (Category cat in cats)
            {
                Context.Categories.Remove(cat);
            }

            await Context.SaveChangesAsync();

            ActionResult result = (await Target.GetCategories()).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task GetCategories_WithZeroPostsForCategory_ShouldNotReturnCategory()
        {
            var emptyCategory = new Category
            {
                Title = "empty",
                UrlSlug = "empty",
                Description = "desc"
            };
            await Context.Categories.AddAsync(emptyCategory);
            await Context.SaveChangesAsync();

            CategoryDto[] result = (await Target.GetCategories()).Value;

            result.Any(c => c.Id == emptyCategory.Id).Should().BeFalse();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithCategoryFoundInStorageAndNoPosts_ShouldReturnNotFound(int id)
        {
            List<Post> posts = await Context.Posts.Where(p => p.CategoryId == id).ToListAsync();
            foreach (Post post in posts)
            {
                Context.Posts.Remove(post);
            }

            await Context.SaveChangesAsync();

            ActionResult result = (await Target.GetPostsByCategory(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(999)]
        public async Task GetPostsByCategory_WithCategoryNotFoundInStorage_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.GetPostsByCategory(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public async Task GetPostsByCategory_WithInvalidId_ShouldReturnNotFound(int id)
        {
            ActionResult result = (await Target.GetPostsByCategory(id)).Result;

            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithPostsInStorage_ShouldNotReturnDeletedPosts(int id)
        {
            Post deletedPost = await Context.Posts.FirstOrDefaultAsync(p => p.IsDeleted && p.CategoryId == id);

            SearchResult<PostDto> result = (await Target.GetPostsByCategory(id)).Value;

            result.Data.Any(p => p.Id == deletedPost.Id).Should().BeFalse();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithPostsInStorage_ShouldNotReturnNotVisiblePosts(int id)
        {
            Post invisiblePost = await Context.Posts.FirstOrDefaultAsync(p => !p.IsVisible && p.CategoryId == id);

            SearchResult<PostDto> result = (await Target.GetPostsByCategory(id)).Value;

            result.Data.Any(p => p.Id == invisiblePost.Id).Should().BeFalse();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithPostsInStorage_ShouldRemoveBodyFromPosts(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByCategory(id)).Value;

            result.Data.First().BodyEnd.Should().BeNullOrEmpty();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithPostsInStorage_ShouldReturnAllAvailablePostsInCategory(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByCategory(id)).Value;

            result.TotalCount.Should().Be(1);
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithPostsInStorage_ShouldReturnPostDtoSearchResult(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByCategory(id)).Value;

            result.Should().NotBeNull();
            result.Should().BeOfType<SearchResult<PostDto>>();
        }

        [Test]
        [TestCase(3)]
        public async Task GetPostsByCategory_WithPostsInStorage_ShouldReturnResultOrderedByCreated(int id)
        {
            SearchResult<PostDto> result = (await Target.GetPostsByCategory(id)).Value;

            Assert.That(result.Data, Is.Ordered.By("Created").Descending);
        }

        #endregion
    }
}
