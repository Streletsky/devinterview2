﻿using System;
using System.Collections.Generic;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Entities.Identity;

namespace DevInterview2.Web.Tests.TestHelpers
{
    public static class FakeDataProvider
    {
        #region Public Methods and Operators

        public static List<Category> PopulateCategoryStore()
        {
            var catStore = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Description = "cat cat",
                    Title = "ASP.NET",
                    UrlSlug = "aspnet"
                },
                new Category
                {
                    Id = 2,
                    Description = "cat 2 cat",
                    Title = "ASP",
                    UrlSlug = "asp"
                },
                new Category
                {
                    Id = 3,
                    Description = "cat 3",
                    Title = ".NET",
                    UrlSlug = "net"
                }
            };

            return catStore;
        }

        public static List<Post> PopulatePostStore()
        {
            var postStore = new List<Post>
            {
                new Post
                {
                    Id = 1,
                    BodyEnd = "end1",
                    BodyStart = "start1",
                    Description = "description1",
                    Created = DateTime.Now,
                    CreatedBy = new AppUser(),
                    IsDeleted = false,
                    IsVisible = true,
                    LastUpdated = DateTime.Now,
                    LastUpdatedBy = new AppUser(),
                    RatingId = 1,
                    TagPosts = new List<TagPosts>
                    {
                        new TagPosts
                        {
                            TagId = 1,
                            PostId = 1
                        }
                    },
                    Title = "title1",
                    UrlSlug = "url1",
                    Views = 100,
                    CategoryId = 1
                },

                new Post
                {
                    Id = 2,
                    BodyEnd = "end2",
                    BodyStart = "start2",
                    Description = "description2",
                    Created = DateTime.Now,
                    CreatedBy = new AppUser(),
                    IsDeleted = false,
                    IsVisible = true,
                    LastUpdated = DateTime.Now,
                    LastUpdatedBy = new AppUser(),
                    RatingId = 2,
                    TagPosts = new List<TagPosts>
                    {
                        new TagPosts
                        {
                            TagId = 1,
                            PostId = 2
                        },
                        new TagPosts
                        {
                            TagId = 2,
                            PostId = 2
                        }
                    },
                    Title = "title2",
                    UrlSlug = "url2",
                    Views = 100,
                    CategoryId = 2
                },
                new Post
                {
                    Id = 3,
                    BodyEnd = "end3",
                    BodyStart = "start3",
                    Description = "description3",
                    Created = DateTime.Now,
                    CreatedBy = new AppUser(),
                    IsDeleted = true,
                    IsVisible = true,
                    LastUpdated = DateTime.Now,
                    LastUpdatedBy = new AppUser(),
                    RatingId = 3,
                    TagPosts = new List<TagPosts>
                    {
                        new TagPosts
                        {
                            TagId = 1,
                            PostId = 3
                        },
                        new TagPosts
                        {
                            TagId = 2,
                            PostId = 3
                        }
                    },
                    Title = "title3",
                    UrlSlug = "url3",
                    Views = 100,
                    CategoryId = 3
                },
                new Post
                {
                    Id = 4,
                    BodyEnd = "end4",
                    BodyStart = "start4",
                    Description = "description4",
                    Created = DateTime.Now,
                    CreatedBy = new AppUser(),
                    IsDeleted = false,
                    IsVisible = false,
                    LastUpdated = DateTime.Now,
                    LastUpdatedBy = new AppUser(),
                    RatingId = 4,
                    TagPosts = new List<TagPosts>
                    {
                        new TagPosts
                        {
                            TagId = 1,
                            PostId = 4
                        },
                        new TagPosts
                        {
                            TagId = 2,
                            PostId = 4
                        }
                    },
                    Title = "title4",
                    UrlSlug = "url4",
                    Views = 100,
                    CategoryId = 3
                },
                new Post
                {
                    Id = 5,
                    BodyEnd = "end5",
                    BodyStart = "start5",
                    Description = "description5",
                    Created = DateTime.Now,
                    CreatedBy = new AppUser(),
                    IsDeleted = false,
                    IsVisible = true,
                    LastUpdated = DateTime.Now,
                    LastUpdatedBy = new AppUser(),
                    RatingId = 5,
                    TagPosts = new List<TagPosts>
                    {
                        new TagPosts
                        {
                            TagId = 1,
                            PostId = 5
                        },
                        new TagPosts
                        {
                            TagId = 2,
                            PostId = 5
                        },
                        new TagPosts
                        {
                            TagId = 3,
                            PostId = 5
                        }
                    },
                    Title = "title5",
                    UrlSlug = "url5",
                    Views = 100,
                    CategoryId = 3
                }
            };

            return postStore;
        }

        public static List<Rating> PopulateRatingStore()
        {
            var tags = new List<Rating>
            {
                new Rating
                {
                    Id = 1,
                    VotesUp = 10,
                    VotesDown = 5
                },
                new Rating
                {
                    Id = 2,
                    VotesUp = 0,
                    VotesDown = 0
                },
                new Rating
                {
                    Id = 3,
                    VotesUp = 0,
                    VotesDown = 3
                },
                new Rating
                {
                    Id = 4,
                    VotesUp = 8,
                    VotesDown = 4
                },
                new Rating
                {
                    Id = 5,
                    VotesUp = 3,
                    VotesDown = 0
                }
            };

            return tags;
        }

        public static List<AppRole> PopulateRoleStore()
        {
            var roleStore = new List<AppRole>
            {
                new AppRole
                {
                    Id = 1,
                    Name = "Administrator"
                },
                new AppRole
                {
                    Id = 2,
                    Name = "Publisher"
                },
                new AppRole
                {
                    Id = 3,
                    Name = "Common"
                }
            };

            return roleStore;
        }

        public static List<Tag> PopulateTagStore()
        {
            var tags = new List<Tag>
            {
                new Tag
                {
                    Id = 1,
                    Title = "title1",
                    UrlSlug = "url1"
                },
                new Tag
                {
                    Id = 2,
                    Title = "title2",
                    UrlSlug = "url2"
                },
                new Tag
                {
                    Id = 3,
                    Title = "title2",
                    UrlSlug = "url2"
                }
            };

            return tags;
        }

        public static List<AppUser> PopulateUserStore()
        {
            var userStore = new List<AppUser>
            {
                new AppUser
                {
                    Id = 1,
                    UserName = "George",
                    Email = "george@hi.com",
                    Created = DateTime.Now,
                    IsActive = false,
                    IsDeleted = true
                },
                new AppUser
                {
                    Id = 2,
                    UserName = "NOONE",
                    Email = "noone@hi.com",
                    Created = DateTime.Now,
                    IsActive = false,
                    IsDeleted = true
                },
                new AppUser
                {
                    Id = 3,
                    UserName = "Axe",
                    Email = "nope@moo.com",
                    Created = DateTime.Now,
                    IsActive = false,
                    IsDeleted = true
                }
            };

            return userStore;
        }

        #endregion
    }
}
