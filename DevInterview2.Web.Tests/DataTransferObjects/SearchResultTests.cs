﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.OrmInfrastructure;
using DevInterview2.Web.DataTransferObjects;
using DevInterview2.Web.Tests.TestHelpers;
using Duende.IdentityServer.EntityFramework.Options;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace DevInterview2.Web.Tests.DataTransferObjects
{
    [TestFixture]
    public class SearchResultTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            var storeOptionsMock = new Mock<IOptions<OperationalStoreOptions>>();
            var storeOptionsValueMock = new Mock<OperationalStoreOptions>();
            storeOptionsMock.SetupGet(o => o.Value).Returns(storeOptionsValueMock.Object);
            DbContextOptions<AppDbContext> options = new DbContextOptionsBuilder<AppDbContext>()
                                                     .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            context = new AppDbContext(options, storeOptionsMock.Object);
            context.AddRange(FakeDataProvider.PopulatePostStore());
            context.SaveChanges();
        }

        #endregion


        #region All other members

        private AppDbContext context;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(2, 2)]
        public async Task Create_WithPagingParams_ShouldReturnPagedSearchResult(int page, int pageSize)
        {
            int postsCount = await context.Posts.CountAsync();

            SearchResult<Post> result = SearchResult<Post>.Create(context.Posts, page, pageSize);

            result.Data.Count().Should().Be(pageSize);
            result.Page.Should().Be(page);
            result.PageSize.Should().Be(pageSize);
            result.TotalCount.Should().Be(postsCount);
            result.TotalPages.Should().Be((int)Math.Ceiling(postsCount / (double)pageSize));
        }

        [Test]
        [TestCase(2, 2)]
        public async Task CreateAsync_WithPagingParams_ShouldReturnPagedSearchResult(int page, int pageSize)
        {
            int postsCount = await context.Posts.CountAsync();

            SearchResult<Post> result = await SearchResult<Post>.CreateAsync(context.Posts, page, pageSize);

            result.Data.Count().Should().Be(pageSize);
            result.Page.Should().Be(page);
            result.PageSize.Should().Be(pageSize);
            result.TotalCount.Should().Be(postsCount);
            result.TotalPages.Should().Be((int)Math.Ceiling(postsCount / (double)pageSize));
        }

        [Test]
        [TestCase(1, 10, "Title", "ASC")]
        public async Task CreateAsync_WithParams_ShouldReturnSearchResultWithParams(
            int page,
            int pageSize,
            string sortColumn,
            string sortOrder)
        {
            SearchResult<Post> result =
                await SearchResult<Post>.CreateAsync(context.Posts, page, pageSize, sortColumn, sortOrder);

            result.Page.Should().Be(page);
            result.PageSize.Should().Be(pageSize);
            result.SortColumn.Should().Be(sortColumn);
            result.SortOrder.Should().Be(sortOrder);
        }

        [Test]
        [TestCase(1, 10, "Title", "ASC")]
        [TestCase(1, 10, "Created", "DESC")]
        public async Task CreateAsync_WithSortingParams_ShouldReturnSortedSearchResult(
            int page,
            int pageSize,
            string sortColumn,
            string sortOrder)
        {
            SearchResult<Post> result =
                await SearchResult<Post>.CreateAsync(context.Posts, page, pageSize, sortColumn, sortOrder);

            Assert.That(result.Data,
                        sortOrder == "ASC"
                            ? Is.Ordered.By(sortColumn).Ascending
                            : Is.Ordered.By(sortColumn).Descending);
        }

        #endregion
    }
}
