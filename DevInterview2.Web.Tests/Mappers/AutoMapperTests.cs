﻿using AutoMapper;
using DevInterview2.Web.DataTransferObjects;
using NUnit.Framework;

namespace DevInterview2.Web.Tests.Mappers
{
    [TestFixture]
    public class AutoMapperTests
    {
        private IMapper target;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new PostDtoProfile());
                cfg.AddProfile(new CategoryDtoProfile());
                cfg.AddProfile(new TagDtoProfile());
                cfg.AddProfile(new RatingDtoProfile());
            });
            
            target = mapper.CreateMapper();
        }


        [Test]
        public void Map_Should_HaveValidConfig()
        {
            target.ConfigurationProvider.AssertConfigurationIsValid();
        }
    }
}