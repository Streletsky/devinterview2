import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { querySelector } from '../../shared/helpers/testing.helpers';
import { WidgetComponent } from '../widget/widget.component';

import { AboutStatsComponent } from './about-stats.component';

describe('AboutStatsComponent', () => {
  const daysAgo: number = Math.ceil((new Date().getTime() - new Date(2016, 1, 1).getTime()) / (1000 * 3600 * 24));

  let component: AboutStatsComponent;
  let fixture: ComponentFixture<AboutStatsComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutStatsComponent, WidgetComponent],
      providers: [{ provide: 'LAUNCH_DATE', useValue: new Date(2016, 1, 1) }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutStatsComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate daysAgo field from launch date', () => {
    expect(component.daysAgo).toBe(daysAgo);
  });

  describe('#DOM render', () => {
    it('should set widget title', () => {
      fixture.detectChanges();

      expect(querySelector('section h5', el)).toBe('About this site');
    });

    it('should interpolate daysAgo into description', () => {
      fixture.detectChanges();

      expect(querySelector('section p:last-child strong', el)).toBe(daysAgo.toString());
    });
  });
});
