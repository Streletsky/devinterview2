import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-about-stats',
  templateUrl: './about-stats.component.html',
  styleUrls: ['./about-stats.component.scss'],
})
export class AboutStatsComponent {
  public daysAgo: number;

  constructor(@Inject('LAUNCH_DATE') private launchDate: Date) {
    this.daysAgo = Math.ceil((new Date().getTime() - launchDate.getTime()) / (1000 * 3600 * 24));
  }
}
