import { NO_ERRORS_SCHEMA, SimpleChange, SimpleChanges } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { getDummyPosts, Post } from '../../shared/models/post.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { SpinnerDirective } from '../directives/spinner/spinner.directive';
import { PaginationComponent } from '../pagination/pagination.component';
import { PostComponent } from '../post/post.component';
import { PostListComponent } from './post-list.component';

const dummySearchResult: SearchResult<Post> = {
  data: getDummyPosts(),
  page: 1,
  pageSize: 10,
  sortColumn: 'Created',
  sortOrder: 'DESC',
  totalCount: 10,
  totalPages: 1,
};

describe('PostListComponent', () => {
  let component: PostListComponent;
  let fixture: ComponentFixture<PostListComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpinnerDirective, PaginationComponent, PostComponent, PostListComponent],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostListComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set pages array from search result', () => {
    const previousValue = null;
    const currentValue = {
      data: getDummyPosts(),
      page: 1,
      pageSize: 2,
      sortColumn: 'Created',
      sortOrder: 'DESC',
      totalCount: 10,
      totalPages: 5,
    };
    const changesObj: SimpleChanges = {
      posts: new SimpleChange(previousValue, currentValue, true),
    };
    component.posts = currentValue;

    fixture.detectChanges();
    component.ngOnChanges(changesObj);

    expect(component.pages?.length).toBe(5);
  });

  describe('#DOM render', () => {
    it('should not generate pagination component when there are no results', () => {
      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(PaginationComponent))).toBeFalsy();
    });

    it('should generate pagination component when there are results', () => {
      component.posts = dummySearchResult;

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(PaginationComponent))).toBeTruthy();
    });

    it('should generate post component for each post', () => {
      component.posts = dummySearchResult;

      fixture.detectChanges();

      expect(fixture.debugElement.queryAll(By.directive(PostComponent)).length).toBe(10);
    });

    it('should show spinner while posts are not loaded', () => {
      component.posts = null;

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(SpinnerDirective))).toBeTruthy();

      component.posts = dummySearchResult;

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(SpinnerDirective))).toBeFalsy();
    });

    it('should not show pagination when showPagination is false', () => {
      component.posts = dummySearchResult;
      component.showPagination = false;

      fixture.detectChanges();

      expect(fixture.debugElement.query(By.directive(PaginationComponent))).toBeFalsy();
    });
  });
});
