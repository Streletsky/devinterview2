import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { Post } from '../../shared/models/post.model';
import { SearchResult } from '../../shared/models/search-result.model';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnChanges {
  @Input() public paginationRouteBase: string | null = null;

  @Input() public showPagination = true;

  @Input() public posts: SearchResult<Post> | null = null;

  public pages: number[] | null = [];

  public ngOnChanges(changes: SimpleChanges) {
    if (changes['posts']) {
      this.pages =
        this.posts &&
        Array(this.posts.totalPages)
          .fill(0)
          .map((v, i) => i + 1);
    }
  }
}
