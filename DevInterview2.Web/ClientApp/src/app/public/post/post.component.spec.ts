import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { querySelector } from '../../shared/helpers/testing.helpers';
import { getPostMock } from '../../shared/models/post.model';
import { PostComponent } from './post.component';

describe('PostComponent', () => {
  let component: PostComponent;
  let fixture: ComponentFixture<PostComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [PostComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#DOM render', () => {
    it('should not display footer if post is not full', () => {
      component.post = getPostMock({ bodyEnd: null });

      fixture.detectChanges();

      expect(el.querySelector('app-similar-posts')).toBeFalsy();
      expect(el.querySelector('app-comments')).toBeFalsy();
    });

    it('should display footer if post is full', () => {
      component.post = getPostMock();

      fixture.detectChanges();

      expect(el.querySelector('app-similar-posts')).toBeTruthy();
      expect(el.querySelector('app-comments')).toBeTruthy();
    });

    it('should add CSS class to article if post is not full', () => {
      component.post = getPostMock({ bodyEnd: null });

      fixture.detectChanges();

      expect(el.querySelector('article')?.className).toContain('border rounded-lg px-3 py-3 mb-4');
    });

    it('should not add CSS class to article if post is full', () => {
      component.post = getPostMock();

      fixture.detectChanges();

      expect(el.querySelector('article')?.className).toContain('mb-4');
    });

    it('should set header in h2 with link when post is not full', () => {
      const post = getPostMock({ bodyEnd: null });
      component.post = post;

      fixture.detectChanges();

      expect(querySelector('h2 a', el)).toBe(post.title);
    });

    it('should set header in h1 when post is full', () => {
      const post = getPostMock();
      component.post = post;

      fixture.detectChanges();

      expect(querySelector('h1', el)).toBe(post.title);
    });

    it('should render tags in footer', () => {
      component.post = getPostMock();

      fixture.detectChanges();

      expect(el.querySelectorAll('app-tag-link').length).toBe(2);
    });

    it('should render created time in footer', () => {
      component.post = getPostMock();

      fixture.detectChanges();

      expect(querySelector('time', el)).toBe('Oct 1, 2020');
    });

    it('should render created time in footer', () => {
      const post = getPostMock();
      component.post = post;

      fixture.detectChanges();

      expect(querySelector('footer a', el)).toBe(post.category.title);
    });

    it('should render body end when post is full', () => {
      const post = getPostMock();
      component.post = post;

      fixture.detectChanges();

      const b = el.querySelector('#postBodyEnd');

      expect(b !== null ? b.innerHTML : null).toBe(post.bodyEnd);
    });

    it('should not render body end when post is full', () => {
      component.post = getPostMock({ bodyEnd: null });

      fixture.detectChanges();

      expect(el.querySelector('#postBodyEnd')).toBeFalsy();
    });
  });
});
