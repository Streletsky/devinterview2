import { Component, Input } from '@angular/core';

import { Post } from '../../shared/models/post.model';
import { UrlComposerService } from '../../shared/services/url-composer.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent {
  @Input()
  public post!: Post;

  constructor(private urlComposerService: UrlComposerService) {}

  public getCategoryUrl() {
    if (!this.post?.category) return;

    return this.urlComposerService.getUrlToCategory(this.post.category);
  }

  public getPostUrl() {
    if (this.post === null) return;

    return this.urlComposerService.getUrlToPost(this.post);
  }
}
