import { Component, Inject, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';

import { Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-top-rated',
  templateUrl: './top-rated.component.html',
  styleUrls: ['./top-rated.component.scss'],
})
export class TopRatedComponent implements OnInit {
  public searchParams: SearchParams;

  public posts$: Observable<SearchResult<Post>> | null = null;

  constructor(
    @Inject('DEFAULT_PAGE_SIZE') private defaultPageSize: number,
    @Inject('DOMAIN_NAME') private domainName: number,
    private postService: PostService,
    private seoService: SeoService
  ) {
    this.searchParams = new SearchParams({
      pageSize: defaultPageSize,
      page: 1,
      sortColumn: 'Rating',
      sortOrder: 'DESC',
    });
  }

  public ngOnInit() {
    this.postService.getPage(this.searchParams).subscribe((posts) => {
      this.posts$ = of(posts);
    });

    this.updateHeadTags();
  }

  private updateHeadTags() {
    this.seoService.setCanonicalUrl();
    this.seoService.setTitle(`Top rated interview questions at ${this.domainName}`);
    this.seoService.setMeta(`Top rated interview questions for software developer position at ${this.domainName}`);
  }
}
