import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';

import { getPostMock } from '../../shared/models/post.model';
import { PostService } from '../../shared/services/post.service';
import { PostPageComponent } from './post-page.component';

const activatedRouteMock = {
  paramMap: of(convertToParamMap({ id: 'what-is-scrum-58' })),
};

const dummyPost = getPostMock({ id: 58 });

describe('PostPageComponent', () => {
  let component: PostPageComponent;
  let fixture: ComponentFixture<PostPageComponent>;
  let route: ActivatedRoute;
  let postService: PostService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostPageComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        { provide: 'API_URL_BASE', useValue: '/api' },
        PostService,
        { provide: ActivatedRoute, useValue: activatedRouteMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostPageComponent);
    component = fixture.componentInstance;
    route = TestBed.inject(ActivatedRoute);
    postService = TestBed.inject(PostService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set postId based on route params', () => {
    activatedRouteMock.paramMap = of(convertToParamMap({ id: 'what-is-scrum-58' }));

    fixture = TestBed.createComponent(PostPageComponent);
    component = fixture.componentInstance;

    component.postId$.subscribe((id) => {
      expect(id).toBe('58');
    });
  });

  describe('#ngOnInit', () => {
    it('should wait for postId from route params', () => {
      spyOn(postService, 'getPost').and.returnValue(of(dummyPost));

      fixture = TestBed.createComponent(PostPageComponent);
      component = fixture.componentInstance;

      component.postId$ = new BehaviorSubject<string | null>(null);
      fixture.detectChanges();

      expect(postService.getPost).toHaveBeenCalledTimes(0);
    });

    it('should call PostService when postId is ready', () => {
      spyOn(postService, 'getPost').and.returnValue(of(dummyPost));

      fixture = TestBed.createComponent(PostPageComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      expect(postService.getPost).toHaveBeenCalledTimes(1);
      expect(postService.getPost).toHaveBeenCalledWith('58');
    });

    it('should set post field to Post', () => {
      spyOn(postService, 'getPost').and.returnValue(of(dummyPost));

      fixture.detectChanges();

      component.post$.subscribe((post) => {
        expect(post).toBeTruthy();
        expect(post?.id).toBe(58);
      });
    });
  });
});
