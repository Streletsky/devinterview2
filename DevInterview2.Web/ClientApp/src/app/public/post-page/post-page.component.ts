import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { skipWhile, switchMap } from 'rxjs/operators';

import { Post } from '../../shared/models/post.model';
import { PostService } from '../../shared/services/post.service';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss'],
})
export class PostPageComponent implements OnInit {
  public post$: BehaviorSubject<Post | null>;

  public postId$: BehaviorSubject<string | null>;

  constructor(private postService: PostService, private route: ActivatedRoute, private seoService: SeoService) {
    this.post$ = new BehaviorSubject<Post | null>(null);
    this.postId$ = new BehaviorSubject<string | null>(null);

    route.paramMap.subscribe((params) => {
      const idSlug = params.get('id');
      let id = null;
      if (idSlug) {
        id = idSlug.split('-').pop() || null;
      }

      this.postId$.next(id);
    });
  }

  public ngOnInit() {
    this.postId$
      .pipe(
        skipWhile((id) => id === null),
        switchMap((id) => this.postService.getPost(id!))
      )
      .subscribe((post) => {
        this.post$.next(post);
        this.updateHeadTags(post);
      });
  }

  private updateHeadTags(post: Post) {
    this.seoService.setCanonicalUrl();
    this.seoService.setTitle(post.title);
    this.seoService.setMeta(post.description);
  }
}
