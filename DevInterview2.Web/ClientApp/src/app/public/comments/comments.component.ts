import { Component } from '@angular/core';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent {
  public pageUrl: string;

  constructor() {
    this.pageUrl = location.href.replace(location.hash, '');
  }
}
