import { Directive, ElementRef, HostBinding } from '@angular/core';

@Directive({
  selector: '[appSpinner]',
})
export class SpinnerDirective {
  @HostBinding('class') private class = 'spinner-grow text-secondary';
  @HostBinding('attr.role') private role = 'status';

  constructor(private el: ElementRef) {
    el.nativeElement.innerHTML = '<span class="sr-only">Loading...</span>';
  }
}
