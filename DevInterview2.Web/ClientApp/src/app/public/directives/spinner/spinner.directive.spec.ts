import { ElementRefMock } from '../../../shared/helpers/testing.helpers';
import { SpinnerDirective } from './spinner.directive';

describe('SpinnerDirective', () => {
  it('should create an instance', () => {
    const directive = new SpinnerDirective(new ElementRefMock());
    expect(directive).toBeTruthy();
  });
});
