import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { skipWhile, switchMap, tap } from 'rxjs/operators';

import { Category } from '../../shared/models/category.model';
import { Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { AppState } from '../../shared/ngrx';
import { selectCategoryByUrl } from '../../shared/ngrx/category.selectors';
import { CategoryService } from '../../shared/services/category.service';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  public searchParams$: BehaviorSubject<SearchParams | null>;

  public posts$: BehaviorSubject<SearchResult<Post> | null>;

  public category$: BehaviorSubject<Category | null>;

  public routeBase$: BehaviorSubject<string | null>;

  constructor(
    @Inject('DEFAULT_PAGE_SIZE') private defaultPageSize: number,
    private route: ActivatedRoute,
    private categoryService: CategoryService,
    private store: Store<AppState>,
    private seoService: SeoService
  ) {
    this.searchParams$ = new BehaviorSubject<SearchParams | null>(null);
    this.category$ = new BehaviorSubject<Category | null>(null);
    this.posts$ = new BehaviorSubject<SearchResult<Post> | null>(null);
    this.routeBase$ = new BehaviorSubject<string | null>(null);

    route.paramMap.subscribe((params) => {
      const currentPage = params.get('page') || 1;
      const categoryUrlSlug = params.get('url');

      this.store.pipe(select(selectCategoryByUrl(categoryUrlSlug))).subscribe((cat) => this.category$.next(cat));
      this.routeBase$.next(`/c/${categoryUrlSlug}`);

      this.searchParams$.next(
        new SearchParams({
          pageSize: defaultPageSize,
          page: +currentPage,
        })
      );
    });
  }

  public ngOnInit() {
    combineLatest([this.searchParams$, this.category$])
      .pipe(
        skipWhile(([params, category]) => params === null || category === null || category === undefined),
        tap(([params, category]) => this.updateHeadTags(category)),
        switchMap(([params, category]) => this.categoryService.getPosts(category!.id, params!))
      )
      .subscribe((posts) => {
        this.posts$.next(posts);
      });
  }

  private updateHeadTags(category: Category | null) {
    if (category === null) return;

    this.seoService.setCanonicalUrl();
    this.seoService.setTitle(category.title);
    this.seoService.setMeta(category.description);
  }
}
