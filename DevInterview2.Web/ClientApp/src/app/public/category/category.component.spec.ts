import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';
import { Category, getCategoryMock } from '../../shared/models/category.model';
import { getDummyPosts, Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { AppState } from '../../shared/ngrx';
import { initialCategoryState } from '../../shared/ngrx/category.reducer';
import { initialTagState } from '../../shared/ngrx/tag.reducer';
import { CategoryService } from '../../shared/services/category.service';

import { CategoryComponent } from './category.component';

const activatedRouteMock = {
  paramMap: of(convertToParamMap({ page: 3, url: 'dot-net' })),
};

const dummySearchResult: SearchResult<Post> = {
  data: getDummyPosts(),
  page: 1,
  pageSize: 10,
  sortColumn: 'Created',
  sortOrder: 'DESC',
  totalCount: 10,
  totalPages: 1,
};

const dummyCategories: Category[] = [getCategoryMock({ urlSlug: 'dot-net', id: 2 })];

describe('CategoryComponent', () => {
  let component: CategoryComponent;
  let fixture: ComponentFixture<CategoryComponent>;
  let store: MockStore;
  let service: CategoryService;
  const initialState: AppState = { category: initialCategoryState, tag: initialTagState };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [CategoryComponent],
      providers: [
        CategoryService,
        provideMockStore({ initialState }),
        { provide: ActivatedRoute, useValue: activatedRouteMock },
        { provide: 'DEFAULT_PAGE_SIZE', useValue: 10 },
        { provide: 'API_URL_BASE', useValue: '/api' },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    service = TestBed.inject(CategoryService);
    store = TestBed.inject(MockStore);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set searchParams based on route params', () => {
    activatedRouteMock.paramMap = of(convertToParamMap({ page: 3, url: 'dot-net' }));
    fixture = TestBed.createComponent(CategoryComponent);
    component = fixture.componentInstance;

    component.searchParams$.subscribe((params) => {
      expect(params?.page).toBe(3);
      expect(params?.pageSize).toBe(10);
    });
  });

  it('should set category from store based on url from route params', () => {
    activatedRouteMock.paramMap = of(convertToParamMap({ page: 3, url: 'dot-net' }));
    fixture = TestBed.createComponent(CategoryComponent);
    component = fixture.componentInstance;
    store.setState({ category: { categories: dummyCategories, error: null } });

    fixture.detectChanges();

    component.category$.subscribe((cat) => {
      expect(cat?.id).toBe(2);
    });
  });

  describe('#ngOnInit', () => {
    it('should getPosts from CategoryService when all params are ready', () => {
      const dummySearchParams = new SearchParams({ page: 3, pageSize: 10 });
      store.setState({ category: { categories: dummyCategories, error: null } });
      activatedRouteMock.paramMap = of(convertToParamMap({ page: 3, url: 'dot-net' }));
      fixture = TestBed.createComponent(CategoryComponent);
      component = fixture.componentInstance;
      spyOn(service, 'getPosts').and.returnValue(of(dummySearchResult));

      fixture.detectChanges();

      expect(service.getPosts).toHaveBeenCalledTimes(1);
      expect(service.getPosts).toHaveBeenCalledWith(2, dummySearchParams);
      component.posts$.subscribe((posts) => expect(posts).toEqual(dummySearchResult));
    });
  });
});
