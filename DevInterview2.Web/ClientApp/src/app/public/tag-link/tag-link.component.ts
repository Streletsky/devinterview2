import { Component, Input } from '@angular/core';

import { Tag } from '../../shared/models/tag.model';
import { UrlComposerService } from '../../shared/services/url-composer.service';

@Component({
  selector: 'app-tag-link',
  templateUrl: './tag-link.component.html',
  styleUrls: ['./tag-link.component.scss'],
})
export class TagLinkComponent {
  @Input() public tag: Tag | null = null;

  @Input() public showCounter = true;

  constructor(private urlComposerService: UrlComposerService) {}

  public getTagUrl(tag: Tag) {
    return this.urlComposerService.getUrlToTag(tag);
  }
}
