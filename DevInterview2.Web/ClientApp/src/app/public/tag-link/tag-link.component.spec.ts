import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { querySelector } from '../../shared/helpers/testing.helpers';
import { getTagMock, Tag } from '../../shared/models/tag.model';

import { TagLinkComponent } from './tag-link.component';

const dummyTag: Tag = getTagMock();

describe('TagLinkComponent', () => {
  let component: TagLinkComponent;
  let fixture: ComponentFixture<TagLinkComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [TagLinkComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagLinkComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#DOM render', () => {
    it('should set link text, href and posts count', () => {
      component.tag = dummyTag;

      fixture.detectChanges();

      expect((el.querySelector('a.btn') as HTMLAnchorElement).href).toContain(`/t/${dummyTag.urlSlug}`);
      expect(querySelector('a.btn span:first-child', el)).toBe(dummyTag.title);
      expect(querySelector('a.btn span.badge', el)).toBe(dummyTag.postsCount.toString());
    });
  });
});
