import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { CategoryComponent } from './category/category.component';
import { HomeComponent } from './home/home.component';
import { MostViewedComponent } from './most-viewed/most-viewed.component';
import { PostPageComponent } from './post-page/post-page.component';
import { SearchComponent } from './search/search.component';
import { TagComponent } from './tag/tag.component';
import { TopRatedComponent } from './top-rated/top-rated.component';

export const publicRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'p/:ignoreParam/:id', component: PostPageComponent, pathMatch: 'full' },
  { path: 'p/:page', component: HomeComponent, pathMatch: 'full' },
  { path: 'p', redirectTo: '', pathMatch: 'full' },
  { path: 'c/:url/:page', component: CategoryComponent, pathMatch: 'full' },
  { path: 'c/:url', component: CategoryComponent, pathMatch: 'full' },
  { path: 't/:url/:page', component: TagComponent, pathMatch: 'full' },
  { path: 't/:url', component: TagComponent, pathMatch: 'full' },
  { path: 'most-viewed', component: MostViewedComponent, pathMatch: 'full' },
  { path: 'top-rated', component: TopRatedComponent, pathMatch: 'full' },
  { path: 'search/:query', component: SearchComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(publicRoutes)],
  exports: [RouterModule],
})
export class PublicRoutingModule {}
