import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DisqusModule } from 'ngx-disqus';

import { SharedModule } from '../shared/shared.module';
import { CategoriesNavComponent } from './categories-nav/categories-nav.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { PostListComponent } from './post-list/post-list.component';
import { PublicRoutingModule } from './public-routing.module';
import { SidebarContainerComponent } from './sidebar-container/sidebar-container.component';
import { TagLinkComponent } from './tag-link/tag-link.component';
import { TagsNavComponent } from './tags-nav/tags-nav.component';
import { WidgetComponent } from './widget/widget.component';
import { AboutStatsComponent } from './about-stats/about-stats.component';
import { SpinnerDirective } from './directives/spinner/spinner.directive';
import { PostComponent } from './post/post.component';
import { PaginationComponent } from './pagination/pagination.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PostInfoComponent } from './post-info/post-info.component';
import { PostRatingComponent } from './post-rating/post-rating.component';
import { SimilarPostsComponent } from './similar-posts/similar-posts.component';
import { CommentsComponent } from './comments/comments.component';
import { CategoryComponent } from './category/category.component';
import { TagComponent } from './tag/tag.component';
import { MostViewedComponent } from './most-viewed/most-viewed.component';
import { TopRatedComponent } from './top-rated/top-rated.component';
import { PostPageComponent } from './post-page/post-page.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { SearchComponent } from './search/search.component';
import { AffiliateDisclosureComponent } from './affiliate-disclosure/affiliate-disclosure.component';

@NgModule({
  declarations: [
    NavMenuComponent,
    HomeComponent,
    FooterComponent,
    SidebarContainerComponent,
    CategoriesNavComponent,
    TagsNavComponent,
    TagLinkComponent,
    WidgetComponent,
    AboutStatsComponent,
    PostListComponent,
    SpinnerDirective,
    PostComponent,
    PaginationComponent,
    NotFoundComponent,
    PostInfoComponent,
    PostRatingComponent,
    SimilarPostsComponent,
    CommentsComponent,
    CategoryComponent,
    TagComponent,
    MostViewedComponent,
    TopRatedComponent,
    PostPageComponent,
    SearchFormComponent,
    SearchComponent,
    AffiliateDisclosureComponent,
  ],
  imports: [DisqusModule, CommonModule, SharedModule, PublicRoutingModule, ReactiveFormsModule],
  providers: [
    { provide: 'DEFAULT_PAGE_SIZE', useValue: 10 },
    { provide: 'DOMAIN_NAME', useValue: 'developer-interview.com' },
    { provide: 'LAUNCH_DATE', useValue: new Date(2016, 1, 1) },
  ],
  exports: [NavMenuComponent, HomeComponent, FooterComponent, SidebarContainerComponent],
})
export class PublicModule {}
