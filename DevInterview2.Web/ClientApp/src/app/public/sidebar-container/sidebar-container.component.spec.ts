import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Category, getCategoryMock } from '../../shared/models/category.model';
import { getTagMock, Tag } from '../../shared/models/tag.model';
import { AppState } from '../../shared/ngrx';
import { TagService } from '../../shared/services/tag.service';
import { SidebarContainerComponent } from './sidebar-container.component';

const dummyCategories: Category[] = [getCategoryMock({ id: 1 }), getCategoryMock({ id: 2 })];

const dummyTags: Tag[] = [getTagMock({ id: 1 }), getTagMock({ id: 2 })];

const dummyState: AppState = {
  category: { categories: dummyCategories, error: null },
  tag: { tags: dummyTags, error: null },
};

describe('SidebarContainerComponent', () => {
  let component: SidebarContainerComponent;
  let fixture: ComponentFixture<SidebarContainerComponent>;
  let tagService: TagService;
  let store: MockStore<AppState>;
  const initialState: AppState = {
    category: {
      categories: [],
      error: null,
    },
    tag: {
      tags: [],
      error: null,
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarContainerComponent],
      imports: [HttpClientTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [TagService, provideMockStore({ initialState }), { provide: 'API_URL_BASE', useValue: '/api' }],
    }).compileComponents();

    tagService = TestBed.inject(TagService);
    store = TestBed.inject(MockStore);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarContainerComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('should load all categories from NgRx store', () => {
      store.setState(dummyState);

      fixture.detectChanges();

      component.categories$.subscribe((cats) => {
        expect(cats.length).toBe(2);
        expect(cats).toEqual(dummyCategories);
      });
    });

    it('should load all tags from NgRx store', () => {
      store.setState(dummyState);

      fixture.detectChanges();

      component.tags$.subscribe((tags) => {
        expect(tags.length).toBe(2);
        expect(tags).toEqual(dummyTags);
      });
    });
  });
});
