import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Category } from '../../shared/models/category.model';
import { Tag } from '../../shared/models/tag.model';
import { AppState } from '../../shared/ngrx';
import { loadCategories } from '../../shared/ngrx/category.actions';
import { selectCategories } from '../../shared/ngrx/category.selectors';
import { loadTags } from '../../shared/ngrx/tag.actions';
import { selectTags } from '../../shared/ngrx/tag.selectors';

@Component({
  selector: 'app-sidebar-container',
  templateUrl: './sidebar-container.component.html',
  styleUrls: ['./sidebar-container.component.scss'],
})
export class SidebarContainerComponent implements OnInit {
  public categories$: Observable<Category[]>;

  public tags$: Observable<Tag[]>;

  constructor(private store: Store<AppState>) {
    this.categories$ = this.store.pipe(select(selectCategories));
    this.tags$ = this.store.pipe(select(selectTags));
  }

  public ngOnInit() {
    this.store.dispatch(loadCategories());
    this.store.dispatch(loadTags());
  }
}
