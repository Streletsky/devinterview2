import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { querySelector } from '../../shared/helpers/testing.helpers';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FooterComponent],
      providers: [{ provide: 'DOMAIN_NAME', useValue: 'developer-interview.com' }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should inject DOMAIN_NAME', () => {
    expect(component.domainName).toBe('developer-interview.com');
  });

  it('should set year to current year', () => {
    expect(component.year).toBe(new Date().getFullYear());
  });

  describe('#DOM render', () => {
    it('should inject year and domain name into footer', () => {
      fixture.detectChanges();

      expect(querySelector('footer div', el)).toContain(new Date().getFullYear().toString());
      expect(querySelector('footer div', el)).toContain('developer-interview.com');
    });
  });
});
