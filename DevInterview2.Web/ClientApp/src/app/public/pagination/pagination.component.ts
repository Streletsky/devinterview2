import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent {
  @Input() public currentPage = 1;

  @Input() public pages: number[] | null = [];

  @Input() public routeBase: string | null = null;
}
