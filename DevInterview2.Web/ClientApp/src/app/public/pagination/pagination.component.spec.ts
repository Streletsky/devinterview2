import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { PaginationComponent } from './pagination.component';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;
  let el: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [PaginationComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#DOM render', () => {
    it('should not render if there are no pages', () => {
      component.currentPage = 1;
      component.pages = [];
      component.routeBase = '/posts';

      fixture.detectChanges();

      expect(el.querySelectorAll('nav').length).toBe(0);
    });

    it('should not render if there is only 1 page', () => {
      component.currentPage = 1;
      component.pages = [1];
      component.routeBase = '/posts';

      fixture.detectChanges();

      expect(el.querySelectorAll('nav').length).toBe(0);
    });

    it('should generate button for each page', () => {
      component.currentPage = 1;
      component.pages = [1, 2, 3, 4, 5, 6];
      component.routeBase = '/posts';

      fixture.detectChanges();

      expect(el.querySelectorAll('nav li.page-item a.page-link').length).toBe(6 + 2); // plus prev and next links
    });

    it('should generate buttons with a link to a route', () => {
      component.currentPage = 3;
      component.pages = [1, 2, 3, 4, 5, 6];
      component.routeBase = '/posts';

      fixture.detectChanges();

      el.querySelectorAll('nav li.page-item a.page-link').forEach((link, index) => {
        const anchor = link as HTMLAnchorElement;

        if (index === 0) {
          // prev button
          expect(anchor.href.endsWith('/posts/2')).toBeTruthy();
          return;
        }

        if (index === 7) {
          // next button
          expect(anchor.href.endsWith('/posts/4')).toBeTruthy();
          return;
        }

        expect(anchor.href.endsWith(`/posts/${index}`)).toBeTruthy();
        expect(anchor.innerText).toBe(index.toString());
      });
    });

    it('should disable Previous button when on the first page', () => {
      component.currentPage = 1;
      component.pages = [1, 2, 3, 4, 5, 6];
      component.routeBase = '/posts';

      fixture.detectChanges();

      expect(el.querySelector('nav li.page-item:first-child')?.classList.contains('disabled')).toBeTruthy();
    });

    it('should disable Next button when on the last page', () => {
      component.currentPage = 6;
      component.pages = [1, 2, 3, 4, 5, 6];
      component.routeBase = '/posts';

      fixture.detectChanges();

      expect(el.querySelector('nav li.page-item:last-child')?.classList.contains('disabled')).toBeTruthy();
    });
  });
});
