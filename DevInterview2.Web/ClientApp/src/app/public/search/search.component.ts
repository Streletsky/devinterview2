import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { skipWhile, switchMap } from 'rxjs/operators';

import { Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  public searchParams$: BehaviorSubject<SearchParams | null>;

  public posts$: BehaviorSubject<SearchResult<Post> | null>;

  constructor(
    @Inject('DEFAULT_PAGE_SIZE') private defaultPageSize: number,
    private route: ActivatedRoute,
    private postService: PostService,
    private seoService: SeoService
  ) {
    this.searchParams$ = new BehaviorSubject<SearchParams | null>(null);
    this.posts$ = new BehaviorSubject<SearchResult<Post> | null>(null);

    route.paramMap.subscribe((params) => {
      const query = params.get('query');

      this.searchParams$.next(
        new SearchParams({
          pageSize: defaultPageSize,
          page: 1,
          searchQuery: query,
        })
      );
    });
  }

  public ngOnInit(): void {
    this.searchParams$
      .pipe(
        skipWhile((params) => params === null),
        switchMap((params) => this.postService.getPage(params))
      )
      .subscribe((posts) => {
        this.posts$.next(posts);
      });

    this.updateHeadTags();
  }

  private updateHeadTags() {
    this.seoService.setCanonicalUrl();
    this.seoService.setTitle('Search results');
    this.seoService.setMeta('Search results');
  }
}
