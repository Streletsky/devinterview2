import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';

import { getDummyPosts, Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';
import { SearchComponent } from './search.component';

const activatedRouteMock = {
  paramMap: of(convertToParamMap({ query: 'javascript' })),
};

const dummySearchResult: SearchResult<Post> = {
  data: getDummyPosts(),
  page: 1,
  pageSize: 10,
  sortColumn: 'Created',
  sortOrder: 'DESC',
  totalCount: 10,
  totalPages: 1,
};

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let el: HTMLElement;
  let route: ActivatedRoute;
  let postService: PostService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [SearchComponent],
      providers: [
        PostService,
        { provide: ActivatedRoute, useValue: activatedRouteMock },
        { provide: 'API_URL_BASE', useValue: '/api' },
        { provide: 'DEFAULT_PAGE_SIZE', useValue: 10 },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
    route = TestBed.inject(ActivatedRoute);
    postService = TestBed.inject(PostService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set searchParams based on route params', () => {
    activatedRouteMock.paramMap = of(convertToParamMap({ query: 'javascript' }));

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    component.searchParams$.subscribe((params) => {
      expect(params?.page).toBe(1);
      expect(params?.pageSize).toBe(10);
      expect(params?.searchQuery).toBe('javascript');
    });
  });

  describe('#ngOnInit', () => {
    it('should not load anything without search params', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));

      fixture = TestBed.createComponent(SearchComponent);
      component = fixture.componentInstance;

      component.searchParams$ = new BehaviorSubject<SearchParams | null>(null);
      fixture.detectChanges();

      expect(postService.getPage).toHaveBeenCalledTimes(0);
    });

    it('should call PostService with params', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));
      const searchParams = new SearchParams({
        page: 1,
        pageSize: 10,
        searchQuery: 'javascript',
      });
      activatedRouteMock.paramMap = of(convertToParamMap({ query: 'javascript' }));

      fixture = TestBed.createComponent(SearchComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      expect(postService.getPage).toHaveBeenCalledTimes(1);
      expect(postService.getPage).toHaveBeenCalledWith(searchParams);
    });

    it('should set posts field to Post[]', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));

      fixture.detectChanges();

      component.posts$.subscribe((posts) => {
        expect(posts).toBeTruthy();
        expect(posts?.data.length).toBe(10);
        expect(posts?.data).toEqual(dummySearchResult.data);
      });
    });
  });
});
