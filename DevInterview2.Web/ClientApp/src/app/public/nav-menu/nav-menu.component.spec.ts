import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavMenuComponent } from './nav-menu.component';

describe('NavMenuComponent', () => {
  let component: NavMenuComponent;
  let fixture: ComponentFixture<NavMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavMenuComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavMenuComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be collapsed by default', () => {
    expect(component.isExpanded).toBeFalsy();
  });

  describe('#collapse', () => {
    it('should collapse', () => {
      component.isExpanded = true;

      component.collapse();

      expect(component.isExpanded).toBeFalsy();
    });
  });

  describe('#toggle', () => {
    it('should switch isExpanded', () => {
      component.isExpanded = false;

      component.toggle();
      expect(component.isExpanded).toBeTruthy();

      component.toggle();
      expect(component.isExpanded).toBeFalsy();
    });
  });
});
