import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { querySelector } from '../../shared/helpers/testing.helpers';
import { Category, getCategoryMock } from '../../shared/models/category.model';
import { WidgetComponent } from '../widget/widget.component';
import { CategoriesNavComponent } from './categories-nav.component';

const dummyCategories: Category[] = [
  getCategoryMock({ id: 1 }),
  getCategoryMock({ id: 2 }),
  getCategoryMock({ id: 3 }),
];

describe('CategoriesNavComponent', () => {
  let component: CategoriesNavComponent;
  let fixture: ComponentFixture<CategoriesNavComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CategoriesNavComponent, WidgetComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesNavComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#DOM render', () => {
    it('should set widget title', () => {
      fixture.detectChanges();

      expect(querySelector('section h5', el)).toBe('Categories');
    });

    it('should create nav link for each Category', () => {
      component.categories = dummyCategories;

      fixture.detectChanges();

      expect(el.querySelectorAll('a.nav-link').length).toBe(dummyCategories.length);
    });

    it('should create nav link with Category name, URL and posts count', () => {
      component.categories = dummyCategories;

      fixture.detectChanges();

      expect((el.querySelector('a.nav-link:first-child') as HTMLAnchorElement).href).toContain(
        `/c/${dummyCategories[0].urlSlug}`
      );
      expect(querySelector('a.nav-link:first-child span:first-child', el)).toBe(dummyCategories[0].title);
      expect(querySelector('a.nav-link:first-child span.badge:last-child', el)).toBe(
        dummyCategories[0].postsCount.toString()
      );
    });
  });
});
