import { Component, Input } from '@angular/core';

import { Category } from '../../shared/models/category.model';
import { UrlComposerService } from '../../shared/services/url-composer.service';

@Component({
  selector: 'app-categories-nav',
  templateUrl: './categories-nav.component.html',
  styleUrls: ['./categories-nav.component.scss'],
})
export class CategoriesNavComponent {
  @Input() public categories!: Category[];

  constructor(private urlComposerService: UrlComposerService) {}

  public getCategoryUrl(category: Category) {
    return this.urlComposerService.getUrlToCategory(category);
  }
}
