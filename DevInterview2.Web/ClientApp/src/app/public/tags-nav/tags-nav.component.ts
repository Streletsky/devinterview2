import { Component, Input } from '@angular/core';
import { Tag } from '../../shared/models/tag.model';

@Component({
  selector: 'app-tags-nav',
  templateUrl: './tags-nav.component.html',
  styleUrls: ['./tags-nav.component.scss'],
})
export class TagsNavComponent {
  @Input() public tags!: Tag[];
}
