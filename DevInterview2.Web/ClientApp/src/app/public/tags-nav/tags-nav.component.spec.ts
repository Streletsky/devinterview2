import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { querySelector } from '../../shared/helpers/testing.helpers';
import { getTagMock, Tag } from '../../shared/models/tag.model';
import { TagLinkComponent } from '../tag-link/tag-link.component';
import { WidgetComponent } from '../widget/widget.component';
import { TagsNavComponent } from './tags-nav.component';

const dummyTags: Tag[] = [getTagMock({ id: 1 }), getTagMock({ id: 2 }), getTagMock({ id: 3 })];

describe('TagsNavComponent', () => {
  let component: TagsNavComponent;
  let fixture: ComponentFixture<TagsNavComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [TagsNavComponent, WidgetComponent, TagLinkComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsNavComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#DOM render', () => {
    it('should set widget title', () => {
      fixture.detectChanges();

      expect(querySelector('section h5', el)).toBe('Tags');
    });

    it('should create tag-link for each Tag', () => {
      component.tags = dummyTags;

      fixture.detectChanges();

      expect(fixture.debugElement.queryAll(By.directive(TagLinkComponent)).length).toBe(dummyTags.length);
    });
  });
});
