import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { skipWhile, switchMap } from 'rxjs/operators';

import { Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public searchParams$: BehaviorSubject<SearchParams | null>;

  public posts$: BehaviorSubject<SearchResult<Post> | null>;

  constructor(
    @Inject('DEFAULT_PAGE_SIZE') private defaultPageSize: number,
    @Inject('DOMAIN_NAME') private domainName: number,
    private route: ActivatedRoute,
    private postService: PostService,
    private seoService: SeoService
  ) {
    this.searchParams$ = new BehaviorSubject<SearchParams | null>(null);
    this.posts$ = new BehaviorSubject<SearchResult<Post> | null>(null);

    route.paramMap.subscribe((params) => {
      const currentPage = params.get('page') || 1;

      this.searchParams$.next(
        new SearchParams({
          pageSize: defaultPageSize,
          page: +currentPage,
        })
      );
    });
  }

  public ngOnInit() {
    this.searchParams$
      .pipe(
        skipWhile((params) => params === null),
        switchMap((params) => this.postService.getPage(params))
      )
      .subscribe((posts) => {
        this.posts$.next(posts);
      });

    this.updateHeadTags();
  }

  private updateHeadTags() {
    this.seoService.setCanonicalUrl();
    this.seoService.setTitle('Software developer interview Q&A - developer-interview.com');
    this.seoService.setMeta(
      'Check out the list of the most common interview questions for software developer position'
    );
  }
}
