﻿import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';

import { getDummyPosts, Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';
import { HomeComponent } from './home.component';

const activatedRouteMock = {
  paramMap: of(convertToParamMap({ page: 3 })),
};

const dummySearchResult: SearchResult<Post> = {
  data: getDummyPosts(),
  page: 1,
  pageSize: 10,
  sortColumn: 'Created',
  sortOrder: 'DESC',
  totalCount: 10,
  totalPages: 1,
};

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let el: HTMLElement;
  let route: ActivatedRoute;
  let postService: PostService;
  let meta: Meta;
  let title: Title;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, BrowserModule],
      declarations: [HomeComponent],
      providers: [
        PostService,
        Meta,
        Title,
        { provide: ActivatedRoute, useValue: activatedRouteMock },
        { provide: 'API_URL_BASE', useValue: '/api' },
        { provide: 'DEFAULT_PAGE_SIZE', useValue: 10 },
        { provide: 'DOMAIN_NAME', useValue: 'developer-interview.com' },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
    route = TestBed.inject(ActivatedRoute);
    postService = TestBed.inject(PostService);
    meta = TestBed.inject(Meta);
    title = TestBed.inject(Title);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set searchParams based on route params', () => {
    activatedRouteMock.paramMap = of(convertToParamMap({ page: 3 }));

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    component.searchParams$.subscribe((params) => {
      expect(params?.page).toBe(3);
      expect(params?.pageSize).toBe(10);
    });
  });

  it('should set title', () => {
    fixture.detectChanges();

    expect(title.getTitle()).toBe('Software developer interview Q&A - developer-interview.com');
  });

  describe('#ngOnInit', () => {
    it('should not load anything without search params', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));

      fixture = TestBed.createComponent(HomeComponent);
      component = fixture.componentInstance;

      component.searchParams$ = new BehaviorSubject<SearchParams | null>(null);
      fixture.detectChanges();

      expect(postService.getPage).toHaveBeenCalledTimes(0);
    });

    it('should call PostService with params', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));
      const searchParams = new SearchParams({
        page: 3,
        pageSize: 10,
      });
      activatedRouteMock.paramMap = of(convertToParamMap({ page: 3 }));

      fixture = TestBed.createComponent(HomeComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      expect(postService.getPage).toHaveBeenCalledTimes(1);
      expect(postService.getPage).toHaveBeenCalledWith(searchParams);
    });

    it('should set posts field to Post[]', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));

      fixture.detectChanges();

      component.posts$.subscribe((posts) => {
        expect(posts).toBeTruthy();
        expect(posts?.data.length).toBe(10);
        expect(posts?.data).toEqual(dummySearchResult.data);
      });
    });
  });
});
