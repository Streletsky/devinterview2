import { Component, Inject, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { skipWhile } from 'rxjs/operators';
import { Post } from '../../shared/models/post.model';

import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-most-viewed',
  templateUrl: './most-viewed.component.html',
  styleUrls: ['./most-viewed.component.scss'],
})
export class MostViewedComponent implements OnInit {
  public searchParams: SearchParams;

  public posts$: Observable<SearchResult<Post>> | null = null;

  constructor(
    @Inject('DEFAULT_PAGE_SIZE') private defaultPageSize: number,
    @Inject('DOMAIN_NAME') private domainName: number,
    private postService: PostService,
    private seoService: SeoService
  ) {
    this.searchParams = new SearchParams({
      pageSize: defaultPageSize,
      page: 1,
      sortColumn: 'Views',
      sortOrder: 'DESC',
    });
  }

  public ngOnInit() {
    this.postService.getPage(this.searchParams).subscribe((posts) => {
      this.posts$ = of(posts);
    });

    this.updateHeadTags();
  }

  private updateHeadTags() {
    this.seoService.setCanonicalUrl();
    this.seoService.setTitle(`Most viewed interview questions at ${this.domainName}`);
    this.seoService.setMeta(`Most viewed interview questions for software developer position at ${this.domainName}`);
  }
}
