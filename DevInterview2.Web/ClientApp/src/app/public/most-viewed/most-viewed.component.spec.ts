import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { getDummyPosts, Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';

import { MostViewedComponent } from './most-viewed.component';

const dummySearchResult: SearchResult<Post> = {
  data: getDummyPosts(),
  page: 1,
  pageSize: 10,
  sortColumn: 'Created',
  sortOrder: 'DESC',
  totalCount: 10,
  totalPages: 1,
};

describe('MostViewedComponent', () => {
  let component: MostViewedComponent;
  let fixture: ComponentFixture<MostViewedComponent>;
  let postService: PostService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [MostViewedComponent],
      providers: [
        PostService,
        { provide: 'API_URL_BASE', useValue: '/api' },
        { provide: 'DEFAULT_PAGE_SIZE', useValue: 10 },
        { provide: 'DOMAIN_NAME', useValue: 'developer-interview.com' },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostViewedComponent);
    component = fixture.componentInstance;
    postService = TestBed.inject(PostService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set searchParams to search by Views', () => {
    fixture.detectChanges();

    const result = component.searchParams;

    expect(result.page).toBe(1);
    expect(result.pageSize).toBe(10);
    expect(result.sortOrder).toBe('DESC');
    expect(result.sortColumn).toBe('Views');
  });

  describe('#ngOnInit', () => {
    it('should call PostService with params', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));
      const searchParams = new SearchParams({
        pageSize: 10,
        page: 1,
        sortColumn: 'Views',
        sortOrder: 'DESC',
      });

      fixture = TestBed.createComponent(MostViewedComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      expect(postService.getPage).toHaveBeenCalledTimes(1);
      expect(postService.getPage).toHaveBeenCalledWith(searchParams);
    });

    it('should set posts field to Post[]', () => {
      spyOn(postService, 'getPage').and.returnValue(of(dummySearchResult));

      fixture.detectChanges();

      component.posts$?.subscribe((posts) => {
        expect(posts).toBeTruthy();
        expect(posts.data.length).toBe(10);
        expect(posts.data).toEqual(dummySearchResult.data);
      });
    });
  });
});
