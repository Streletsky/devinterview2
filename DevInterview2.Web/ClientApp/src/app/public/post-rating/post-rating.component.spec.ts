import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs';

import { RatingVector } from '../../shared/models/rating.model';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import { RatingService } from '../../shared/services/rating.service';
import { PostRatingComponent } from './post-rating.component';

describe('PostRatingComponent', () => {
  let component: PostRatingComponent;
  let fixture: ComponentFixture<PostRatingComponent>;
  let el: HTMLElement;
  let localStorageService: LocalStorageService;
  let ratingService: RatingService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PostRatingComponent],
      providers: [{ provide: 'API_URL_BASE', useValue: '/api' }, RatingService, LocalStorageService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostRatingComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
    localStorageService = TestBed.inject(LocalStorageService);
    ratingService = TestBed.inject(RatingService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('should get vote from LocalStorageService', () => {
      spyOn(localStorageService, 'get').and.returnValue(null);
      component.postId = 1;

      fixture.detectChanges();

      expect(localStorageService.get).toHaveBeenCalledTimes(1);
      expect(localStorageService.get).toHaveBeenCalledWith('rating-1');
    });

    it('should set canVote & votedUp if vote was not found in LocalStorageService', () => {
      spyOn(localStorageService, 'get').and.returnValue(null);
      component.postId = 1;

      fixture.detectChanges();

      expect(component.canVote).toBeTruthy();
      expect(component.votedUp).toBeFalsy();
    });

    it('should set canVote & votedUp from stored vote if vote was found in LocalStorageService', () => {
      spyOn(localStorageService, 'get').and.returnValue(RatingVector.Up);
      component.postId = 1;

      fixture.detectChanges();

      expect(component.canVote).toBeFalsy();
      expect(component.votedUp).toBeTruthy();
    });
  });

  describe('#votePost', () => {
    it('should call votePost in RatingService', () => {
      spyOn(ratingService, 'votePost').and.returnValue(of());
      component.postId = 1;
      component.rating = {
        id: 1,
        votesUp: 0,
        votesDown: 0,
        votesDifference: 0,
      };
      component.canVote = true;

      component.votePost(true);

      expect(ratingService.votePost).toHaveBeenCalledTimes(1);
      expect(ratingService.votePost).toHaveBeenCalledWith(1, true);
    });

    it('should save vote in local storage', () => {
      spyOn(ratingService, 'votePost').and.returnValue(of({}));
      spyOn(localStorageService, 'set');
      component.postId = 1;
      component.rating = {
        id: 1,
        votesUp: 0,
        votesDown: 0,
        votesDifference: 0,
      };
      component.canVote = true;

      component.votePost(true);

      expect(localStorageService.set).toHaveBeenCalledTimes(1);
      expect(localStorageService.set).toHaveBeenCalledWith('rating-1', RatingVector.Up);
    });

    it('should update component fields with vote up', () => {
      spyOn(ratingService, 'votePost').and.returnValue(of({}));
      spyOn(localStorageService, 'set');
      component.postId = 1;
      component.rating = {
        id: 1,
        votesUp: 0,
        votesDown: 0,
        votesDifference: 0,
      };
      component.canVote = true;

      component.votePost(true);

      expect(component.canVote).toBeFalsy();
      expect(component.votedUp).toBeTruthy();
      expect(component.rating.votesUp).toBe(1);
      expect(component.rating.votesDown).toBe(0);
      expect(component.rating.votesDifference).toBe(1);
    });

    it('should update component fields with vote down', () => {
      spyOn(ratingService, 'votePost').and.returnValue(of({}));
      spyOn(localStorageService, 'set');
      component.postId = 1;
      component.rating = {
        id: 1,
        votesUp: 0,
        votesDown: 0,
        votesDifference: 0,
      };
      component.canVote = true;

      component.votePost(false);

      expect(component.canVote).toBeFalsy();
      expect(component.votedUp).toBeFalsy();
      expect(component.rating.votesUp).toBe(0);
      expect(component.rating.votesDown).toBe(1);
      expect(component.rating.votesDifference).toBe(-1);
    });
  });
});
