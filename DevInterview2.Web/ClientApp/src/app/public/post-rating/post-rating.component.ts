import { Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Rating, RatingVector } from '../../shared/models/rating.model';
import { LocalStorageService } from '../../shared/services/local-storage.service';
import { RatingService } from '../../shared/services/rating.service';

@Component({
  selector: 'app-post-rating',
  templateUrl: './post-rating.component.html',
  styleUrls: ['./post-rating.component.scss'],
})
export class PostRatingComponent implements OnInit {
  @Input() public rating: Rating | null = null;

  @Input() public postId = 0;

  @HostBinding('class') private hostClass = 'd-inline-block';

  public canVote = false;

  public votedUp: boolean | null = false;

  private readonly storageKeyPrefix = 'rating-';

  constructor(private ratingService: RatingService, private localStorageService: LocalStorageService) {}

  public ngOnInit() {
    const voteInStorage = this.localStorageService.get(this.getStorageKey());

    if (voteInStorage) {
      this.canVote = false;

      if (voteInStorage === RatingVector.Up) {
        this.votedUp = true;
      } else if (voteInStorage === RatingVector.Down) {
        this.votedUp = false;
      }
    } else {
      this.canVote = true;
    }
  }

  public votePost(voteUp: boolean) {
    if (!this.canVote) {
      return;
    }

    this.ratingService.votePost(this.postId, voteUp).subscribe(() => {
      this.localStorageService.set(this.getStorageKey(), voteUp ? RatingVector.Up : RatingVector.Down);

      this.canVote = false;
      this.votedUp = voteUp;
      this.rating!.votesUp += voteUp ? 1 : 0;
      this.rating!.votesDown += voteUp ? 0 : 1;
      this.rating!.votesDifference = this.rating!.votesUp - this.rating!.votesDown;
    });
  }

  private getStorageKey() {
    return this.storageKeyPrefix + this.postId;
  }
}
