import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { querySelector } from '../../shared/helpers/testing.helpers';
import { getPostMock } from '../../shared/models/post.model';

import { PostInfoComponent } from './post-info.component';

describe('PostInfoComponent', () => {
  let component: PostInfoComponent;
  let fixture: ComponentFixture<PostInfoComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [PostInfoComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostInfoComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#DOM render', () => {
    it('should render views in badge', () => {
      const post = getPostMock();
      component.post = post;

      fixture.detectChanges();

      expect(querySelector('.post-views', el)).toBe(post.views.toString());
    });

    it('should render link to comments', () => {
      component.post = getPostMock();

      fixture.detectChanges();

      expect(el.querySelector('a')?.href).toContain('/p/cat-url/post-url-1#disqus_thread');
    });

    it('should render link for Disqus comments counter', () => {
      component.post = getPostMock();

      fixture.detectChanges();

      expect(el.querySelector('.disqus-comment-count')?.attributes.getNamedItem('data-disqus-url')?.value).toContain(
        '/p/cat-url/post-url-1'
      );
    });
  });
});
