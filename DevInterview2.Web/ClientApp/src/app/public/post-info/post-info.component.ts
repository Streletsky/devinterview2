import { Component, Input, OnInit } from '@angular/core';
import { AuthorizeService } from '../../admin/authorize.service';
import { Post } from '../../shared/models/post.model';
import { UrlComposerService } from '../../shared/services/url-composer.service';

@Component({
  selector: 'app-post-info',
  templateUrl: './post-info.component.html',
  styleUrls: ['./post-info.component.scss'],
})
export class PostInfoComponent implements OnInit {
  @Input() public post!: Post;

  public isAuthorized = false;

  constructor(private urlComposerService: UrlComposerService, private authService: AuthorizeService) {}

  public ngOnInit() {
    this.authService.isAuthenticated().subscribe((isAuthenticated) => {
      this.isAuthorized = isAuthenticated;
    });
  }

  public getPostUrl(isAbsolute = false) {
    if (!this.post) return null;

    return this.urlComposerService.getUrlToPost(this.post, isAbsolute);
  }
}
