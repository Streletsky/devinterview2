import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { querySelector } from '../../shared/helpers/testing.helpers';

import { WidgetComponent } from './widget.component';

describe('WidgetComponent', () => {
  let component: WidgetComponent;
  let fixture: ComponentFixture<WidgetComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#DOM render', () => {
    it('should set widget title', () => {
      component.title = 'Title';

      fixture.detectChanges();

      expect(querySelector('section h5', el)).toBe('Title');
    });
  });
});
