import { Component, Input, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Post } from '../../shared/models/post.model';
import { PostService } from '../../shared/services/post.service';
import { UrlComposerService } from '../../shared/services/url-composer.service';

@Component({
  selector: 'app-similar-posts',
  templateUrl: './similar-posts.component.html',
  styleUrls: ['./similar-posts.component.scss'],
})
export class SimilarPostsComponent implements OnInit {
  @Input() public postId = 0;

  public posts$: Observable<Post[] | null>;

  constructor(private postService: PostService, private urlComposerService: UrlComposerService) {
    this.posts$ = of(null);
  }

  public ngOnInit() {
    this.postService.getSimilar(this.postId).subscribe((posts) => (this.posts$ = of(posts.data)));
  }

  public getPostUrl(post: Post) {
    return this.urlComposerService.getUrlToPost(post);
  }
}
