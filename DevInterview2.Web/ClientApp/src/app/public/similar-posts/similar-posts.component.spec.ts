import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { querySelector } from '../../shared/helpers/testing.helpers';
import { getDummyPosts, getPostMock, Post } from '../../shared/models/post.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { PostService } from '../../shared/services/post.service';
import { UrlComposerService } from '../../shared/services/url-composer.service';
import { SimilarPostsComponent } from './similar-posts.component';

const dummySearchResult: SearchResult<Post> = {
  data: getDummyPosts(),
  page: 1,
  pageSize: 10,
  sortColumn: 'Created',
  sortOrder: 'DESC',
  totalCount: 10,
  totalPages: 1,
};

describe('SimilarPostsComponent', () => {
  let component: SimilarPostsComponent;
  let fixture: ComponentFixture<SimilarPostsComponent>;
  let postService: PostService;
  let urlService: UrlComposerService;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [SimilarPostsComponent],
      providers: [{ provide: 'API_URL_BASE', useValue: '/api' }, PostService, UrlComposerService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimilarPostsComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;

    postService = TestBed.inject(PostService);
    urlService = TestBed.inject(UrlComposerService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#ngOnInit', () => {
    it('should getSimilar from PostService with postId', () => {
      spyOn(postService, 'getSimilar').and.returnValue(of(dummySearchResult));
      component.postId = 1;

      fixture.detectChanges();

      expect(postService.getSimilar).toHaveBeenCalledTimes(1);
      expect(postService.getSimilar).toHaveBeenCalledWith(1);
      component.posts$.subscribe((posts) => expect(posts).toEqual(dummySearchResult.data));
    });

    it('should show spinner while posts are not loaded', fakeAsync(() => {
      spyOn(postService, 'getSimilar').and.returnValue(of(dummySearchResult).pipe(delay(50)));

      fixture.detectChanges();

      component.posts$.subscribe((posts) => expect(posts).toBeFalsy());
      expect(el.querySelector('[appspinner]')).toBeTruthy();
      tick(50);
    }));

    it('should render posts list when posts are loaded', fakeAsync(() => {
      spyOn(postService, 'getSimilar').and.returnValue(of(dummySearchResult).pipe(delay(50)));

      fixture.detectChanges();
      tick(50);

      fixture.detectChanges();

      expect(component.posts$).toBeTruthy();
      expect(el.querySelector('[appspinner]')).toBeFalsy();
      expect(el.querySelector('nav')).toBeTruthy();
      expect(el.querySelectorAll('nav ul li a').length).toBe(dummySearchResult.data.length);
    }));

    it('should render links to post with text and URL', fakeAsync(() => {
      spyOn(postService, 'getSimilar').and.returnValue(of(dummySearchResult).pipe(delay(50)));
      spyOn(urlService, 'getUrlToPost').and.returnValue('/p/cat/post-new-1');

      fixture.detectChanges();
      tick(50);

      fixture.detectChanges();

      expect((el.querySelector('nav ul li a:first-child') as HTMLAnchorElement).href).toContain('/p/cat/post-new-1');
      expect(querySelector('nav ul li a:first-child', el)).toBe(dummySearchResult.data[0].title);
    }));
  });
});
