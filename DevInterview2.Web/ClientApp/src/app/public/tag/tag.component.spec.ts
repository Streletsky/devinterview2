import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { of } from 'rxjs';

import { Tag, getTagMock } from '../../shared/models/tag.model';
import { getDummyPosts, Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { AppState } from '../../shared/ngrx';
import { initialCategoryState } from '../../shared/ngrx/category.reducer';
import { initialTagState } from '../../shared/ngrx/tag.reducer';
import { TagService } from '../../shared/services/tag.service';
import { TagComponent } from './tag.component';

const activatedRouteMock = {
  paramMap: of(convertToParamMap({ page: 3, url: 'basic' })),
};

const dummySearchResult: SearchResult<Post> = {
  data: getDummyPosts(),
  page: 1,
  pageSize: 10,
  sortColumn: 'Created',
  sortOrder: 'DESC',
  totalCount: 10,
  totalPages: 1,
};

const dummyTags: Tag[] = [getTagMock({ urlSlug: 'basic', id: 2 })];

describe('TagComponent', () => {
  let component: TagComponent;
  let fixture: ComponentFixture<TagComponent>;
  let el: HTMLElement;
  let route: ActivatedRoute;
  let store: MockStore;
  let service: TagService;
  const initialState: AppState = { category: initialCategoryState, tag: initialTagState };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      declarations: [TagComponent],
      providers: [
        TagService,
        provideMockStore({ initialState }),
        { provide: ActivatedRoute, useValue: activatedRouteMock },
        { provide: 'DEFAULT_PAGE_SIZE', useValue: 10 },
        { provide: 'API_URL_BASE', useValue: '/api' },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    service = TestBed.inject(TagService);
    route = TestBed.inject(ActivatedRoute);
    store = TestBed.inject(MockStore);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set searchParams based on route params', () => {
    activatedRouteMock.paramMap = of(convertToParamMap({ page: 3, url: 'basic' }));
    fixture = TestBed.createComponent(TagComponent);
    component = fixture.componentInstance;

    component.searchParams$.subscribe((params) => {
      expect(params?.page).toBe(3);
      expect(params?.pageSize).toBe(10);
    });
  });

  it('should set tag from store based on url from route params', () => {
    activatedRouteMock.paramMap = of(convertToParamMap({ page: 3, url: 'basic' }));
    fixture = TestBed.createComponent(TagComponent);
    component = fixture.componentInstance;
    store.setState({ tag: { tags: dummyTags, error: null } });

    fixture.detectChanges();

    component.tag$.subscribe((tag) => {
      expect(tag?.id).toBe(2);
    });
  });

  describe('#ngOnInit', () => {
    it('should getPosts from TagService when all params are ready', () => {
      const dummySearchParams = new SearchParams({ page: 3, pageSize: 10 });
      store.setState({ tag: { tags: dummyTags, error: null } });
      activatedRouteMock.paramMap = of(convertToParamMap({ page: 3, url: 'basic' }));
      fixture = TestBed.createComponent(TagComponent);
      component = fixture.componentInstance;
      spyOn(service, 'getPosts').and.returnValue(of(dummySearchResult));

      fixture.detectChanges();

      expect(service.getPosts).toHaveBeenCalledTimes(1);
      expect(service.getPosts).toHaveBeenCalledWith(2, dummySearchParams);
      component.posts$.subscribe((posts) => expect(posts).toEqual(dummySearchResult));
    });
  });
});
