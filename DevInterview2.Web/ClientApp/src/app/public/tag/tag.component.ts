import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { skipWhile, switchMap, tap } from 'rxjs/operators';

import { Tag } from '../../shared/models/tag.model';
import { Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { SearchResult } from '../../shared/models/search-result.model';
import { AppState } from '../../shared/ngrx';
import { selectTagByUrl } from '../../shared/ngrx/tag.selectors';
import { SeoService } from '../../shared/services/seo.service';
import { TagService } from '../../shared/services/tag.service';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss'],
})
export class TagComponent implements OnInit {
  public searchParams$: BehaviorSubject<SearchParams | null>;

  public posts$: BehaviorSubject<SearchResult<Post> | null>;

  public tag$: BehaviorSubject<Tag | null>;

  public routeBase$: BehaviorSubject<string | null>;

  constructor(
    @Inject('DEFAULT_PAGE_SIZE') private defaultPageSize: number,
    private route: ActivatedRoute,
    private tagService: TagService,
    private store: Store<AppState>,
    private seoService: SeoService
  ) {
    this.searchParams$ = new BehaviorSubject<SearchParams | null>(null);
    this.tag$ = new BehaviorSubject<Tag | null>(null);
    this.posts$ = new BehaviorSubject<SearchResult<Post> | null>(null);
    this.routeBase$ = new BehaviorSubject<string | null>(null);

    route.paramMap.subscribe((params) => {
      const currentPage = params.get('page') || 1;
      const tagUrlSlug = params.get('url');

      this.store.pipe(select(selectTagByUrl(tagUrlSlug))).subscribe((tag) => this.tag$.next(tag));
      this.routeBase$.next(`/t/${tagUrlSlug}`);

      this.searchParams$.next(
        new SearchParams({
          pageSize: defaultPageSize,
          page: +currentPage,
        })
      );
    });
  }

  public ngOnInit() {
    combineLatest([this.searchParams$, this.tag$])
      .pipe(
        skipWhile(([params, tag]) => params === null || tag === null || tag === undefined),
        tap(([params, tag]) => this.updateHeadTags(tag)),
        switchMap(([params, tag]) => this.tagService.getPosts(tag ? tag.id : 0, params))
      )
      .subscribe((posts) => {
        this.posts$.next(posts);
      });
  }

  private updateHeadTags(tag: Tag | null) {
    if (tag === null) return;

    this.seoService.setCanonicalUrl();
    this.seoService.setTitle(`Posts tagged with ${tag.title}`);
    this.seoService.setMeta(`Posts tagged with ${tag.title}`);
  }
}
