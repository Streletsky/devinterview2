import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { publicRoutes } from '../public-routing.module';
import { SearchFormComponent } from './search-form.component';

const routerSpy = { navigate: jasmine.createSpy('navigate') };

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;
  let el: HTMLElement;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule.withRoutes(publicRoutes)],
      declarations: [SearchFormComponent],
      providers: [FormBuilder],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
    router = TestBed.inject(Router);
    router.initialNavigation();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#search', () => {
    it('should be disabled if query is shorter than 3 chars', () => {
      fixture.detectChanges();

      expect(el.querySelector('form button')?.attributes).toContain('disabled');

      component.searchForm.setValue({ query: 'c#' });
      fixture.detectChanges();

      expect(el.querySelector('form button')?.attributes).toContain('disabled');
    });

    it('should be enabled if query is longer than 2 chars', () => {
      component.searchForm.setValue({ query: 'net' });

      fixture.detectChanges();

      expect(el.querySelector('form button')?.attributes).not.toContain('disabled');
    });

    it('should navigate to search on click', () => {
      spyOn(component, 'search').and.callThrough();
      spyOn(router, 'navigate');
      component.searchForm.setValue({ query: 'net' });

      fixture.detectChanges();

      const button = el.querySelector('form button') as HTMLButtonElement;
      button.click();

      expect(component.search).toHaveBeenCalledTimes(1);
      expect(router.navigate).toHaveBeenCalledTimes(1);
      expect(router.navigate).toHaveBeenCalledWith(['search', 'net']);
    });
  });
});
