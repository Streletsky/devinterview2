﻿import { ElementRef } from '@angular/core';

export class ElementRefMock extends ElementRef {
  public override nativeElement: HTMLElement;

  constructor() {
    super(undefined);

    this.nativeElement = {} as HTMLElement;
  }
}

export const querySelector = (selector: string, el: HTMLElement): string =>
  (el.querySelector(selector) as HTMLElement).innerText;
