import { createReducer, on } from '@ngrx/store';

import { Tag } from '../models/tag.model';
import { loadTagsFailure, loadTagsSuccess } from './tag.actions';

export const tagFeatureKey = 'tag';

export interface TagState {
  tags: Tag[];
  error: string | null;
}

export const initialTagState: TagState = {
  tags: [],
  error: null,
};

export const tagReducer = createReducer(
  initialTagState,
  on(loadTagsSuccess, (state, action) => ({ ...state, tags: action.data })),
  on(loadTagsFailure, (state, action) => ({ ...state, error: action.error }))
);
