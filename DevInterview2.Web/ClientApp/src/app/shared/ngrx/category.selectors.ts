import { createSelector } from '@ngrx/store';

import { categoryFeatureKey, CategoryState } from './category.reducer';
import { AppState } from './index';

export const selectFeature = (state: AppState) => state[categoryFeatureKey];

export const selectCategories = createSelector(selectFeature, (state: CategoryState) => state.categories);

export const selectCategoryById = (id: number) =>
  createSelector(selectCategories, (categories) => categories.filter((cat) => cat.id === id));

export const selectCategoryByUrl = (url: string | null) =>
  createSelector(selectCategories, (categories) => categories.find((cat) => cat.urlSlug === url) || null);
