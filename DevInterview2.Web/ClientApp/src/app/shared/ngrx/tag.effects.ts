import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { TagService } from '../services/tag.service';
import { loadTags, loadTagsFailure, loadTagsSuccess } from './tag.actions';

@Injectable()
export class TagEffects {
  public loadTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadTags),
      mergeMap(() =>
        this.tagService.getAll().pipe(
          map((cats) => loadTagsSuccess({ data: cats })),
          catchError((error) => of(loadTagsFailure({ error })))
        )
      )
    )
  );

  constructor(private actions$: Actions, private tagService: TagService) {}
}
