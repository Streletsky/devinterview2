import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { CategoryService } from '../services/category.service';
import { loadCategories, loadCategoriesFailure, loadCategoriesSuccess } from './category.actions';

@Injectable()
export class CategoryEffects {
  public loadCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadCategories),
      mergeMap(() =>
        this.categoryService.getAll().pipe(
          map((cats) => loadCategoriesSuccess({ data: cats })),
          catchError((error) => of(loadCategoriesFailure({ error })))
        )
      )
    )
  );

  constructor(private actions$: Actions, private categoryService: CategoryService) {}
}
