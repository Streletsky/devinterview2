import { createAction, props } from '@ngrx/store';

import { Tag } from '../models/tag.model';

export const loadTags = createAction('[Tag] Load Tags');

export const loadTagsSuccess = createAction('[Tag] Load Tags Success', props<{ data: Tag[] }>());

export const loadTagsFailure = createAction('[Tag] Load Tags Failure', props<{ error: string | null }>());
