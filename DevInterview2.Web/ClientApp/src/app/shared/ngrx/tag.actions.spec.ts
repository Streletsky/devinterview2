import { getTagMock } from '../models/tag.model';
import * as fromTag from './tag.actions';

describe('Tag Actions', () => {
  describe('#loadTags', () => {
    it('should return an action', () => {
      expect(fromTag.loadTags().type).toBe('[Tag] Load Tags');
    });
  });

  describe('#loadTagsSuccess', () => {
    it('should return an action', () => {
      const tags = [getTagMock()];

      const result = fromTag.loadTagsSuccess({ data: tags });

      expect(result.type).toBe('[Tag] Load Tags Success');
      expect(result.data).toBe(tags);
    });
  });

  describe('#loadTagsFailure', () => {
    it('should return an action', () => {
      const error = 'error';

      const result = fromTag.loadTagsFailure({ error });

      expect(result.type).toBe('[Tag] Load Tags Failure');
      expect(result.error).toBe(error);
    });
  });
});
