import { getTagMock } from '../models/tag.model';
import { loadTagsFailure, loadTagsSuccess } from './tag.actions';
import { tagReducer, initialTagState } from './tag.reducer';

describe('Tag Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as never;

      const result = tagReducer(initialTagState, action);

      expect(result).toBe(initialTagState);
    });
  });

  describe('#loadTagsSuccess', () => {
    it('should save Tags into state', () => {
      const action = loadTagsSuccess({ data: [getTagMock()] });

      const result = tagReducer(initialTagState, action);

      expect(result.tags).toBe(action.data);
    });
  });

  describe('#loadTagsFailure', () => {
    it('should set error in state', () => {
      const action = loadTagsFailure({ error: 'error' });

      const result = tagReducer(initialTagState, action);

      expect(result.error).toBe(action.error);
    });
  });
});
