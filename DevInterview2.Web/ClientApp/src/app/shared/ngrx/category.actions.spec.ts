import { getCategoryMock } from '../models/category.model';
import * as fromCategory from './category.actions';

describe('Category Actions', () => {
  describe('#loadCategories', () => {
    it('should return an action', () => {
      expect(fromCategory.loadCategories().type).toBe('[Category] Load Categories');
    });
  });

  describe('#loadCategoriesSuccess', () => {
    it('should return an action', () => {
      const categories = [getCategoryMock()];

      const result = fromCategory.loadCategoriesSuccess({ data: categories });

      expect(result.type).toBe('[Category] Load Categories Success');
      expect(result.data).toBe(categories);
    });
  });

  describe('#loadCategoriesFailure', () => {
    it('should return an action', () => {
      const error = 'error';

      const result = fromCategory.loadCategoriesFailure({ error });

      expect(result.type).toBe('[Category] Load Categories Failure');
      expect(result.error).toBe(error);
    });
  });
});
