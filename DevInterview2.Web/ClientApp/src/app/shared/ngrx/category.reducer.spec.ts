import { getCategoryMock } from '../models/category.model';
import { loadCategoriesFailure, loadCategoriesSuccess } from './category.actions';
import { categoryReducer, initialCategoryState } from './category.reducer';

describe('Category Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as never;

      const result = categoryReducer(initialCategoryState, action);

      expect(result).toBe(initialCategoryState);
    });
  });

  describe('#loadCategoriesSuccess', () => {
    it('should save categories into state', () => {
      const action = loadCategoriesSuccess({ data: [getCategoryMock()] });

      const result = categoryReducer(initialCategoryState, action);

      expect(result.categories).toBe(action.data);
    });
  });

  describe('#loadCategoriesFailure', () => {
    it('should set error in state', () => {
      const action = loadCategoriesFailure({ error: 'error' });

      const result = categoryReducer(initialCategoryState, action);

      expect(result.error).toBe(action.error);
    });
  });
});
