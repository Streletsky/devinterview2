import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { TypedAction } from '@ngrx/store/src/models';
import { Observable, of, throwError } from 'rxjs';

import { getTagMock } from '../models/tag.model';
import { TagService } from '../services/tag.service';
import { loadTags, loadTagsFailure, loadTagsSuccess } from './tag.actions';
import { TagEffects } from './tag.effects';

describe('TagEffects', () => {
  let actions$: Observable<TypedAction<'[Tag] Load Tags'>>;
  let effects: TagEffects;
  let service: TagService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        TagService,
        TagEffects,
        { provide: 'API_URL_BASE', useValue: '/api' },
        provideMockActions(() => actions$),
      ],
    });

    effects = TestBed.inject(TagEffects);
    service = TestBed.inject(TagService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('#loadTags', () => {
    it('should call TagService on loadTags action', (done) => {
      spyOn(service, 'getAll').and.returnValue(of([]));
      actions$ = of(loadTags());

      effects.loadTags$.subscribe(() => {
        expect(service.getAll).toHaveBeenCalledTimes(1);
        done();
      });
    });

    it('should dispatch success action with service call result', (done) => {
      const data = [getTagMock()];
      spyOn(service, 'getAll').and.returnValue(of(data));
      actions$ = of(loadTags());

      effects.loadTags$.subscribe((action) => {
        expect(action).toEqual(loadTagsSuccess({ data }));
        done();
      });
    });

    it('should dispatch failure action on error', (done) => {
      spyOn(service, 'getAll').and.returnValue(throwError('error'));
      actions$ = of(loadTags());

      effects.loadTags$.subscribe((action) => {
        expect(action).toEqual(loadTagsFailure({ error: 'error' }));
        done();
      });
    });
  });
});
