import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { Observable, of, throwError } from 'rxjs';

import { getCategoryMock } from '../models/category.model';
import { CategoryService } from '../services/category.service';
import { loadCategories, loadCategoriesFailure, loadCategoriesSuccess } from './category.actions';
import { CategoryEffects } from './category.effects';

describe('CategoryEffects', () => {
  let actions$: Observable<Action>;
  let effects: CategoryEffects;
  let service: CategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CategoryService,
        CategoryEffects,
        { provide: 'API_URL_BASE', useValue: '/api' },
        provideMockActions(() => actions$),
      ],
    });

    effects = TestBed.inject(CategoryEffects);
    service = TestBed.inject(CategoryService);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('#loadCategories', () => {
    it('should call CategoryService on loadCategories action', (done) => {
      spyOn(service, 'getAll').and.returnValue(of([]));
      actions$ = of(loadCategories());

      effects.loadCategories$.subscribe((action) => {
        expect(service.getAll).toHaveBeenCalledTimes(1);
        done();
      });
    });

    it('should dispatch success action with service call result', (done) => {
      const data = [getCategoryMock()];
      spyOn(service, 'getAll').and.returnValue(of(data));
      actions$ = of(loadCategories());

      effects.loadCategories$.subscribe((action) => {
        expect(action).toEqual(loadCategoriesSuccess({ data }));
        done();
      });
    });

    it('should dispatch failure action on error', (done) => {
      spyOn(service, 'getAll').and.returnValue(throwError('error'));
      actions$ = of(loadCategories());

      effects.loadCategories$.subscribe((action) => {
        expect(action).toEqual(loadCategoriesFailure({ error: 'error' }));
        done();
      });
    });
  });
});
