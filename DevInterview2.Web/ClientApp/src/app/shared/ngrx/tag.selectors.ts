import { createSelector } from '@ngrx/store';

import { tagFeatureKey, TagState } from './tag.reducer';
import { AppState } from './index';

export const selectFeature = (state: AppState) => state[tagFeatureKey];

export const selectTags = createSelector(selectFeature, (state: TagState) => state.tags);

export const selectTagById = (id: number) => createSelector(selectTags, (tags) => tags.filter((cat) => cat.id === id));

export const selectTagByUrl = (url: string | null) =>
  createSelector(selectTags, (tags) => tags.find((tag) => tag.urlSlug === url) || null);
