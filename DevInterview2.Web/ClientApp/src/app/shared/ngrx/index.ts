import { Action, ActionReducer, ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { categoryFeatureKey, categoryReducer, CategoryState } from './category.reducer';
import { tagFeatureKey, tagReducer, TagState } from './tag.reducer';

export interface AppState {
  [categoryFeatureKey]: CategoryState;
  [tagFeatureKey]: TagState;
}

export const reducers: ActionReducerMap<AppState> = {
  [categoryFeatureKey]: categoryReducer,
  [tagFeatureKey]: tagReducer,
};

export const logger =
  (reducer: ActionReducer<AppState>): ActionReducer<AppState, Action> =>
  (state: AppState | undefined, action: Action): AppState => {
    const result = reducer(state, action);

    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();

    return reducer(state, action);
  };

export const metaReducers = !environment.production ? [logger] : [];
