import { createReducer, on } from '@ngrx/store';
import { Category } from '../models/category.model';
import { loadCategoriesFailure, loadCategoriesSuccess } from './category.actions';

export const categoryFeatureKey = 'category';

export interface CategoryState {
  categories: Category[];
  error: string | null;
}

export const initialCategoryState: CategoryState = {
  categories: [],
  error: null,
};

export const categoryReducer = createReducer(
  initialCategoryState,
  on(loadCategoriesSuccess, (state, action) => ({ ...state, categories: action.data })),
  on(loadCategoriesFailure, (state, action) => ({ ...state, error: action.error }))
);
