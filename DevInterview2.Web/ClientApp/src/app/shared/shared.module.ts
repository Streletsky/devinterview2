import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { metaReducers, reducers } from './ngrx';
import { CategoryEffects } from './ngrx/category.effects';
import { TagEffects } from './ngrx/tag.effects';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

@NgModule({
  declarations: [SafeHtmlPipe],
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([CategoryEffects, TagEffects]),
  ],
  providers: [{ provide: 'API_URL_BASE', useValue: '/api' }],
  exports: [SafeHtmlPipe],
})
export class SharedModule {}
