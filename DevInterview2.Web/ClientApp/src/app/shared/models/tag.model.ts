﻿export interface Tag {
  id: number;
  postsCount: number;
  title: string;
  urlSlug: string;
}

const mockDefaults: Tag = {
  id: 1,
  title: 'tag title',
  urlSlug: 'tag-url',
  postsCount: 20,
};

export const getTagMock = (p?: Partial<Tag>): Tag => ({
  ...mockDefaults,
  ...p,
});
