﻿import { HttpParams } from '@angular/common/http';

export class SearchParams {
  public id: number | null = null;
  public page = 1;
  public pageSize = 10;
  public sortColumn: string | null = null;
  public sortOrder: string | null = null;
  public searchQuery: string | null = null;
  public showInvisible: boolean | null = null;

  constructor(init?: Partial<SearchParams>) {
    Object.assign(this, init);
  }

  public get httpParams(): HttpParams {
    return new HttpParams({ fromString: this.urlParamsString });
  }

  private get urlParamsString(): string {
    const params = [`page=${this.page}`, `pageSize=${this.pageSize}`];

    if (this.id) {
      params.push(`id=${this.id}`);
    }
    if (this.sortColumn) {
      params.push(`sortColumn=${this.sortColumn}`);
    }
    if (this.sortOrder) {
      params.push(`sortOrder=${this.sortOrder}`);
    }
    if (this.searchQuery) {
      params.push(`searchQuery=${this.searchQuery}`);
    }
    if (this.showInvisible) {
      params.push(`showInvisible=${this.showInvisible}`);
    }

    return params.join('&');
  }
}
