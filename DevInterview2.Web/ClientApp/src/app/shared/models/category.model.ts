export interface Category {
  id: number;
  description: string;
  postsCount: number;
  title: string;
  urlSlug: string;
}

const mockDefaults: Category = {
  id: 1,
  title: 'cat title',
  urlSlug: 'cat-url',
  description: 'desc',
  postsCount: 20,
};

export const getCategoryMock = (p?: Partial<Category>): Category => ({
  ...mockDefaults,
  ...p,
});
