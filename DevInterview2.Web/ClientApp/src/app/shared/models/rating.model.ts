﻿export interface Rating {
  id: number;
  votesDifference: number;
  votesUp: number;
  votesDown: number;
}

export enum RatingVector {
  Up = 'UP',
  Down = 'DOWN',
}

const mockDefaults: Rating = {
  id: 1,
  votesDifference: 5,
  votesDown: 0,
  votesUp: 5,
};

export const getRatingMock = (p?: Partial<Rating>): Rating => ({
  ...mockDefaults,
  ...p,
});
