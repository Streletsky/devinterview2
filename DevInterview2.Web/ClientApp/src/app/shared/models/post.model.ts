﻿import { Category, getCategoryMock } from './category.model';
import { getRatingMock, Rating } from './rating.model';
import { getTagMock, Tag } from './tag.model';

export interface Post {
  id: number;
  bodyEnd: string | null;
  bodyStart: string;
  category: Category | null;
  created: Date;
  description: string;
  isDeleted: boolean;
  isVisible: boolean;
  isLinkingRequired: boolean;
  lastUpdate: Date;
  rating: Rating | null;
  tags: Tag[];
  title: string;
  urlSlug: string;
  views: number;
}

const mockDefaults: Post = {
  id: 1,
  bodyEnd: '<p>body end</p>',
  bodyStart: '<p>body start</p>',
  category: getCategoryMock(),
  created: new Date(2020, 9, 1, 10, 10, 10),
  description: 'post desc',
  isDeleted: false,
  isLinkingRequired: false,
  isVisible: false,
  lastUpdate: new Date(2020, 12, 1, 10, 10, 10),
  rating: getRatingMock(),
  tags: [getTagMock({ id: 1 }), getTagMock({ id: 2 })],
  title: 'post title',
  urlSlug: 'post-url',
  views: 10,
};

export const getPostMock = (p?: Partial<Post>): Post => ({
  ...mockDefaults,
  ...p,
});

export const getDummyPosts = () => [
  getPostMock({ id: 1 }),
  getPostMock({ id: 2 }),
  getPostMock({ id: 3 }),
  getPostMock({ id: 4 }),
  getPostMock({ id: 5 }),
  getPostMock({ id: 6 }),
  getPostMock({ id: 7 }),
  getPostMock({ id: 8 }),
  getPostMock({ id: 9 }),
  getPostMock({ id: 10 }),
];
