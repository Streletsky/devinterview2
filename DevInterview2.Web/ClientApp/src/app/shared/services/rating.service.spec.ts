import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { RatingService } from './rating.service';

describe('RatingService', () => {
  let service: RatingService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: 'API_URL_BASE', useValue: '/api' }],
    });

    service = TestBed.inject(RatingService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#votePost', () => {
    it('should make API request', () => {
      service.votePost(1, true).subscribe();

      const req = httpMock.expectOne('/api/post/1/vote');

      expect(req.request.method).toBe('PUT');
      expect(req.request.body).toEqual(true);

      req.flush(null);
    });
  });
});
