import { Injectable } from '@angular/core';
import { Category } from '../models/category.model';
import { Post } from '../models/post.model';
import { Tag } from '../models/tag.model';

@Injectable({
  providedIn: 'root',
})
export class UrlComposerService {
  public getUrlToPost(post: Post, isAbsolute = false) {
    return (isAbsolute ? window.location.origin : '') + `/p/${post.category?.urlSlug}/${post.urlSlug}-${post.id}`;
  }

  public getUrlToCategory(category: Category) {
    return `/c/${category.urlSlug}`;
  }

  public getUrlToTag(tag: Tag) {
    return `/t/${tag.urlSlug}`;
  }
}
