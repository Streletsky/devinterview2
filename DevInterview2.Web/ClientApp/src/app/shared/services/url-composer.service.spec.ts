import { TestBed } from '@angular/core/testing';
import { getCategoryMock } from '../models/category.model';
import { getPostMock } from '../models/post.model';
import { getTagMock } from '../models/tag.model';

import { UrlComposerService } from './url-composer.service';

describe('UrlComposerService', () => {
  let service: UrlComposerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlComposerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getUrlToPost', () => {
    it('should generate route link from Post', () => {
      const post = getPostMock();

      const result = service.getUrlToPost(post);

      expect(result).toBe('/p/cat-url/post-url-1');
    });
  });

  describe('#getUrlToCategory', () => {
    it('should generate route link from Category', () => {
      const category = getCategoryMock();

      const result = service.getUrlToCategory(category);

      expect(result).toBe('/c/cat-url');
    });
  });

  describe('#getUrlToTag', () => {
    it('should generate route link from Tag', () => {
      const tag = getTagMock();

      const result = service.getUrlToTag(tag);

      expect(result).toBe('/t/tag-url');
    });
  });
});
