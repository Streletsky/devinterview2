import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { SearchResult } from '../models/search-result.model';
import { SearchParams } from '../models/search-params.model';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  private routePath = '/post';

  constructor(@Inject('API_URL_BASE') private apiUrl: string, private http: HttpClient) {}

  public getPage(searchParams: SearchParams | null = null) {
    if (!searchParams) {
      return this.http.get<SearchResult<Post>>(this.apiUrl + this.routePath);
    } else {
      return this.http.get<SearchResult<Post>>(this.apiUrl + this.routePath, { params: searchParams.httpParams });
    }
  }

  public getPost(id: string) {
    return this.http.get<Post>(`${this.apiUrl}${this.routePath}/${id}`);
  }

  public getSimilar(postId: number) {
    return this.http.get<SearchResult<Post>>(`${this.apiUrl}${this.routePath}/${postId}/similar`);
  }

  public savePost(post: Post) {
    return this.http.post<Post>(`${this.apiUrl}${this.routePath}`, post);
  }

  public updatePost(post: Post) {
    return this.http.put<Post>(`${this.apiUrl}${this.routePath}/${post.id}`, post);
  }

  public deletePost(postId: number) {
    return this.http.delete(`${this.apiUrl}${this.routePath}/${postId}`);
  }
}
