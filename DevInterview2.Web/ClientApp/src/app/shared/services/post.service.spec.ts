import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { getDummyPosts, Post } from '../models/post.model';
import { SearchParams } from '../models/search-params.model';
import { SearchResult } from '../models/search-result.model';

import { PostService } from './post.service';

describe('PostService', () => {
  let service: PostService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: 'API_URL_BASE', useValue: '/api' }],
    });
    service = TestBed.inject(PostService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getPage', () => {
    it('should make API request without params', () => {
      service.getPage().subscribe();

      const req = httpMock.expectOne('/api/post');

      expect(req.request.method).toBe('GET');
      expect(req.request.params.has('page')).toBeFalsy();
      expect(req.request.params.has('pageSize')).toBeFalsy();
      expect(req.request.params.has('sortColumn')).toBeFalsy();
      expect(req.request.params.has('sortOrder')).toBeFalsy();
      expect(req.request.params.has('searchQuery')).toBeFalsy();
      expect(req.request.params.has('showInvisible')).toBeFalsy();

      req.flush(null);
    });

    it('should make API request with params', () => {
      const params = new SearchParams({
        page: 2,
        pageSize: 5,
        searchQuery: 'query',
        showInvisible: true,
        sortColumn: 'title',
        sortOrder: 'asc',
      });

      service.getPage(params).subscribe();

      const req = httpMock.expectOne((r) => r.url.startsWith('/api/post'));

      expect(req.request.method).toBe('GET');
      expect(req.request.params.has('page')).toBeTruthy();
      expect(req.request.params.has('pageSize')).toBeTruthy();
      expect(req.request.params.has('searchQuery')).toBeTruthy();
      expect(req.request.params.has('showInvisible')).toBeTruthy();
      expect(req.request.params.has('sortColumn')).toBeTruthy();
      expect(req.request.params.has('sortOrder')).toBeTruthy();
      expect(req.request.params.get('page')).toBe('2');
      expect(req.request.params.get('pageSize')).toBe('5');
      expect(req.request.params.get('searchQuery')).toBe('query');
      expect(req.request.params.get('showInvisible')).toBe('true');
      expect(req.request.params.get('sortColumn')).toBe('title');
      expect(req.request.params.get('sortOrder')).toBe('asc');

      req.flush(null);
    });

    it('should return an Observable<SearchResult<Post>>', () => {
      const dummySearchResult: SearchResult<Post> = {
        data: getDummyPosts(),
        page: 1,
        pageSize: 10,
        sortColumn: 'Created',
        sortOrder: 'DESC',
        totalCount: 10,
        totalPages: 1,
      };
      const params = new SearchParams({
        page: 2,
        pageSize: 5,
        searchQuery: 'query',
        showInvisible: true,
        sortColumn: 'title',
        sortOrder: 'asc',
      });

      service.getPage(params).subscribe((posts) => {
        expect(posts).toEqual(dummySearchResult);
      });

      const req = httpMock.expectOne((r) => r.url.startsWith('/api/post'));
      req.flush(dummySearchResult);
    });
  });

  describe('#getSimilar', () => {
    it('should make API request with post id', () => {
      service.getSimilar(1).subscribe();

      const req = httpMock.expectOne('/api/post/1/similar');

      expect(req.request.method).toBe('GET');

      req.flush(null);
    });

    it('should return an Observable<SearchResult<Post>>', () => {
      const dummySearchResult: SearchResult<Post> = {
        data: getDummyPosts(),
        page: 1,
        pageSize: 10,
        sortColumn: 'Created',
        sortOrder: 'DESC',
        totalCount: 10,
        totalPages: 1,
      };

      service.getSimilar(1).subscribe((posts) => {
        expect(posts).toEqual(dummySearchResult);
      });

      const req = httpMock.expectOne('/api/post/1/similar');
      req.flush(dummySearchResult);
    });
  });
});
