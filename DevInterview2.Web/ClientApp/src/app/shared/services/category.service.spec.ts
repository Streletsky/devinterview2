import { Category, getCategoryMock } from '../models/category.model';
import { getDummyPosts, Post } from '../models/post.model';
import { SearchParams } from '../models/search-params.model';
import { SearchResult } from '../models/search-result.model';
import { CategoryService } from './category.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

describe('CategoryService', () => {
  let service: CategoryService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: 'API_URL_BASE', useValue: '/api' }],
    });

    service = TestBed.inject(CategoryService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#getAll', () => {
    it('should make API request', () => {
      service.getAll().subscribe();

      const req = httpMock.expectOne('/api/category');

      expect(req.request.method).toBe('GET');

      req.flush(null);
    });

    it('should return an Observable<Category[]>', () => {
      const dummyCategories: Category[] = [getCategoryMock({ id: 1 }), getCategoryMock({ id: 1 })];

      service.getAll().subscribe((cats) => {
        expect(cats.length).toBe(2);
        expect(cats).toEqual(dummyCategories);
      });

      const req = httpMock.expectOne('/api/category');
      req.flush(dummyCategories);
    });
  });

  describe('#getPosts', () => {
    it('should make API request without params', () => {
      service.getPosts(1).subscribe();

      const req = httpMock.expectOne('/api/category/1/posts');

      expect(req.request.method).toBe('GET');
      expect(req.request.params.has('page')).toBeFalsy();
      expect(req.request.params.has('pageSize')).toBeFalsy();
      expect(req.request.params.has('sortColumn')).toBeFalsy();
      expect(req.request.params.has('sortOrder')).toBeFalsy();
      expect(req.request.params.has('searchQuery')).toBeFalsy();
      expect(req.request.params.has('showInvisible')).toBeFalsy();

      req.flush(null);
    });

    it('should make API request with params', () => {
      const params = new SearchParams({
        page: 2,
        pageSize: 5,
      });

      service.getPosts(1, params).subscribe();

      const req = httpMock.expectOne('/api/category/1/posts?page=2&pageSize=5');

      expect(req.request.method).toBe('GET');
      expect(req.request.params.has('page')).toBeTruthy();
      expect(req.request.params.has('pageSize')).toBeTruthy();
      expect(req.request.params.get('page')).toBe('2');
      expect(req.request.params.get('pageSize')).toBe('5');

      req.flush(null);
    });

    it('should return an Observable<SearchResult<Post>>', () => {
      const dummySearchResult: SearchResult<Post> = {
        data: getDummyPosts(),
        page: 1,
        pageSize: 10,
        sortColumn: 'Created',
        sortOrder: 'DESC',
        totalCount: 10,
        totalPages: 1,
      };

      service.getPosts(1).subscribe((posts) => {
        expect(posts).toEqual(dummySearchResult);
      });

      const req = httpMock.expectOne('/api/category/1/posts');
      req.flush(dummySearchResult);
    });
  });
});
