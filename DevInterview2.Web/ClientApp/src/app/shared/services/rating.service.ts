import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RatingService {
  private routePath = '/post';

  constructor(@Inject('API_URL_BASE') private apiUrl: string, private http: HttpClient) {}

  public votePost(postId: number, voteUp: boolean) {
    return this.http.put(`${this.apiUrl + this.routePath}/${postId}/vote/${voteUp}`, null);
  }
}
