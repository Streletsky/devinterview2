import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Category } from '../models/category.model';
import { Post } from '../models/post.model';
import { SearchParams } from '../models/search-params.model';
import { SearchResult } from '../models/search-result.model';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  private routePath = '/category';

  constructor(@Inject('API_URL_BASE') private apiUrl: string, private http: HttpClient) {}

  public getAll() {
    return this.http.get<Category[]>(this.apiUrl + this.routePath);
  }

  public getPosts(categoryId: number, searchParams: SearchParams | null = null) {
    if (!searchParams) {
      return this.http.get<SearchResult<Post>>(`${this.apiUrl + this.routePath}/${categoryId}/posts`);
    } else {
      return this.http.get<SearchResult<Post>>(`${this.apiUrl + this.routePath}/${categoryId}/posts`, {
        params: searchParams.httpParams,
      });
    }
  }

  public saveCategory(category: Category) {
    return this.http.post<Category>(`${this.apiUrl}${this.routePath}`, category);
  }

  public updateCategory(category: Category) {
    return this.http.put<Category>(`${this.apiUrl}${this.routePath}/${category.id}`, category);
  }

  public deleteCategory(categoryId: number) {
    return this.http.delete(`${this.apiUrl}${this.routePath}/${categoryId}`);
  }
}
