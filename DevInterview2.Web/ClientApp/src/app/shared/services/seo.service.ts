import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class SeoService {
  constructor(@Inject(DOCUMENT) private doc: Document, private meta: Meta, private title: Title) {}

  public setCanonicalUrl(url?: string) {
    const link: HTMLLinkElement = this.doc.createElement('link');
    link.setAttribute('rel', 'canonical');

    const existLink = this.doc.head.querySelectorAll('[rel="canonical"]')[0];
    if (existLink) {
      this.doc.head.replaceChild(link, existLink);
    } else {
      this.doc.head.appendChild(link);
    }

    link.setAttribute('href', url ?? this.doc.URL);
  }

  public setTitle(title: string) {
    this.title.setTitle(title);
  }

  public setMeta(description: string) {
    const descriptionMeta = this.meta.getTag('name="description"');

    if (descriptionMeta) {
      this.meta.updateTag(
        {
          content: description,
          name: 'description',
        },
        'name="description"'
      );
    } else {
      this.meta.addTag({
        content: description,
        name: 'description',
      });
    }
  }
}
