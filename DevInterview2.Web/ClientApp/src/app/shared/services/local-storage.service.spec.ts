import { TestBed } from '@angular/core/testing';

import { LocalStorageService } from './local-storage.service';

describe('LocalStorageService', () => {
  let service: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageService);
  });

  afterEach(() => {
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#set', () => {
    it('should set item in localStorage', () => {
      service.set('key', 'value');

      expect(localStorage.getItem('key')).toBe('value');
    });
  });

  describe('#get', () => {
    it('should get item from localStorage', () => {
      localStorage.setItem('key2', 'value2');

      expect(service.get('key2')).toBe('value2');
    });

    it('should return null when item not found in localStorage', () => {
      expect(service.get('key3')).toBeNull();
    });
  });
});
