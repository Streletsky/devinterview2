import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { SearchParams } from '../models/search-params.model';
import { SearchResult } from '../models/search-result.model';
import { Tag } from '../models/tag.model';

@Injectable({
  providedIn: 'root',
})
export class TagService {
  private routePath = '/tag';

  constructor(@Inject('API_URL_BASE') private apiUrl: string, private http: HttpClient) {}

  public getAll() {
    return this.http.get<Tag[]>(this.apiUrl + this.routePath);
  }

  public getPosts(categoryId: number, searchParams: SearchParams | null = null) {
    if (!searchParams) {
      return this.http.get<SearchResult<Post>>(`${this.apiUrl + this.routePath}/${categoryId}/posts`);
    } else {
      return this.http.get<SearchResult<Post>>(`${this.apiUrl + this.routePath}/${categoryId}/posts`, {
        params: searchParams.httpParams,
      });
    }
  }
}
