import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { DISQUS_SHORTNAME } from 'ngx-disqus';
import { AdminModule } from './admin/admin.module';
import { AuthorizeInterceptor } from './admin/authorize.interceptor';

import { AppComponent } from './app.component';
import { PublicModule } from './public/public.module';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    PublicModule,
    AdminModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: DISQUS_SHORTNAME, useValue: 'developerinterview' },
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
