import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular';
// @ts-ignore
import * as Editor from '../../../ckeditor/build/ckeditor';
import { DropdownSettings } from 'angular2-multiselect-dropdown/lib/multiselect.interface';
import { Category } from '../../shared/models/category.model';
import { Post } from '../../shared/models/post.model';
import { SearchParams } from '../../shared/models/search-params.model';
import { Tag } from '../../shared/models/tag.model';
import { CategoryService } from '../../shared/services/category.service';
import { PostService } from '../../shared/services/post.service';
import { TagService } from '../../shared/services/tag.service';
import { AuthorizeService } from '../authorize.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.scss'],
})
export class PostEditComponent implements OnInit {
  public post: Post | null = null;
  public similarPosts: Post[] = [];
  public ckEditor: any = Editor;

  public multiselectSettings: DropdownSettings = {
    text: 'Select Tags',
    labelKey: 'title',
    primaryKey: 'id',
  };

  public postEditForm = this.fb.group({
    title: ['', [Validators.required]],
    description: ['', [Validators.required]],
    bodyStart: ['', [Validators.required]],
    bodyEnd: ['', [Validators.required]],
    isVisible: [false, [Validators.required]],
    category: new FormControl<Category | null>(null, [Validators.required]),
    tags: new FormControl<Tag[]>([], [Validators.required]),
  });

  public editorConfig: CKEditor5.Config = {};

  public categories: Category[] = [];
  public tags: Tag[] = [];

  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
    private router: Router,
    private categoriesService: CategoryService,
    private tagsService: TagService,
    private fb: FormBuilder
  ) {}

  public ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      const id = params.get('id');

      if (id) {
        this.postService.getPost(id).subscribe((post) => {
          this.post = post;
          this.postEditForm.patchValue(post);
        });
      } else {
        this.post = {
          id: 0,
          title: '',
          description: '',
          bodyStart: '',
          bodyEnd: '',
          isVisible: false,
          category: null,
          tags: [],
          created: new Date(),
          lastUpdate: new Date(),
          isDeleted: false,
          isLinkingRequired: false,
          rating: null,
          urlSlug: '',
          views: 0,
        };
      }
    });

    this.categoriesService.getAll().subscribe((categories) => {
      this.categories = categories;
    });

    this.tagsService.getAll().subscribe((tags) => {
      this.tags = tags;
    });
  }

  public savePost() {
    if (this.postEditForm.valid && this.post) {
      this.post.title = this.postEditForm.value.title!;
      this.post.description = this.postEditForm.value.description!;
      this.post.bodyStart = this.postEditForm.value.bodyStart!;
      this.post.bodyEnd = this.postEditForm.value.bodyEnd!;
      this.post.isVisible = this.postEditForm.value.isVisible!;
      this.post.category = this.postEditForm.value.category!;
      this.post.tags = this.postEditForm.value.tags!;

      if (this.post.id) {
        this.postService.updatePost(this.post).subscribe((post) => {
          this.router.navigate(['/']);
        });
      } else {
        this.postService.savePost(this.post).subscribe((post) => {
          this.router.navigate(['/']);
        });
      }
    }
  }

  public deletePost() {
    if (this.post?.id) {
      this.postService.deletePost(this.post.id).subscribe(() => {
        this.router.navigate(['/']);
      });
    }
  }

  public getSimilarPosts() {
    if (this.postEditForm.value.title) {
      this.postService
        .getPage(
          new SearchParams({
            searchQuery: this.postEditForm.value.title,
            pageSize: 5,
            page: 1,
          })
        )
        .subscribe((posts) => {
          this.similarPosts = posts.data;
        });
    }
  }
}
