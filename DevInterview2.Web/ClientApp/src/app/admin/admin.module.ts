import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AdminRoutingModule } from './admin-routing.module';
import { LoginMenuComponent } from './login-menu/login-menu.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { PostEditComponent } from './post-edit/post-edit.component';
import { AdminComponent } from './admin/admin.component';

@NgModule({
  declarations: [PostEditComponent, AdminComponent, LoginMenuComponent, LoginComponent, LogoutComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    AngularMultiSelectModule,
  ],
  exports: [LoginMenuComponent, LoginComponent, LogoutComponent],
})
export class AdminModule {}
