﻿using AutoMapper;
using DevInterview2.Common.Extensions;
using DevInterview2.Common.Helpers;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business.Interfaces;
using DevInterview2.Services.SearchEngine;
using DevInterview2.Web.DataTransferObjects;
using Duende.IdentityServer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DevInterview2.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostController : ControllerBase
    {
        #region Fields

        private readonly IBotDetector botDetector;

        private readonly ILogger<PostController> logger;

        private readonly IMapper mapper;

        private readonly IPostRepository postRepository;

        private readonly IPostSearchEngine postSearchEngine;

        private readonly ITagRepository tagRepository;

        private readonly ICategoryRepository categoryRepository;

        private readonly IUrlizer urlizer;

        #endregion


        #region Constructors and Destructors

        public PostController(ILogger<PostController> logger,
                              IPostRepository postRepository,
                              IBotDetector botDetector,
                              IMapper mapper,
                              IPostSearchEngine postSearchEngine,
                              ITagRepository tagRepository,
                              ICategoryRepository categoryRepository,
                              IUrlizer urlizer)
        {
            this.logger = logger;
            this.postRepository = postRepository;
            this.botDetector = botDetector;
            this.mapper = mapper;
            this.postSearchEngine = postSearchEngine;
            this.tagRepository = tagRepository;
            this.categoryRepository = categoryRepository;
            this.urlizer = urlizer;
        }

        #endregion


        #region Public Methods and Operators

        [HttpGet("{id:int}")]
        public async Task<ActionResult<PostDto>> Get(int id)
        {
            if (id <= 0)
            {
                return new NotFoundResult();
            }

            Post post = await postRepository.GetAsync(id);

            if (post == null || post.IsDeleted)
            {
                return new NotFoundResult();
            }

            var cookieValue = Request.Cookies["ViewedPosts"];
            bool isCrawler = botDetector.CheckUserAgentForBot(Request.Headers["User-Agent"].ToString());
            if (!isCrawler && (cookieValue == null || !cookieValue.Split(",").Contains(post.Id.ToString())))
            {
                var cookieOptions = new CookieOptions();
                cookieOptions.Expires = DateTime.Now.AddDays(1);
                cookieOptions.Path = "/";

                if (cookieValue == null)
                {
                    cookieValue = post.Id.ToString();
                }
                else
                {
                    cookieValue += "," + post.Id;
                }
                
                Response.Cookies.Append("ViewedPosts", cookieValue, cookieOptions);
                
                post.Views += 1;
                await postRepository.UpdateAsync(post);
            }

            return mapper.Map<PostDto>(post);
        }

        [HttpGet]
        public async Task<ActionResult<SearchResult<PostDto>>> GetPosts(
            int page = 1,
            int pageSize = 10,
            string sortColumn = "Created",
            string sortOrder = "DESC",
            string searchQuery = null,
            bool showInvisible = false)
        {
            IQueryable<Post> posts = postRepository.GetAllAvailable(showInvisible);

            SearchResult<Post> result;
            if (!searchQuery.IsNullOrEmpty())
            {
                List<int> postIds = postSearchEngine.Search(searchQuery, pageSize).ToList();
                if (!postIds.Any())
                {
                    return new NotFoundResult();
                }

                List<Post> postsByQuery = await posts.Where(p => postIds.Contains(p.Id)).ToListAsync();
                postsByQuery = postsByQuery.OrderBy(p => postIds.IndexOf(p.Id)).ToList();
                result = SearchResult<Post>.Create(postsByQuery, page, pageSize);
            }
            else
            {
                result = await GetSearchResult(posts, page, pageSize, sortColumn, sortOrder);
            }

            if (result.TotalCount == 0)
            {
                return new NotFoundResult();
            }

            // we don't need second half of body when we query posts list
            foreach (Post p in result.Data)
            {
                p.BodyEnd = null;
            }

            return result.ToDto<PostDto>(mapper);
        }

        [HttpGet("{id:int}/similar")]
        public async Task<ActionResult<SearchResult<PostDto>>> GetSimilarPosts(int id, int page = 1, int pageSize = 5)
        {
            Post post = await postRepository.GetAsync(id);

            if (post == null)
            {
                return new NotFoundResult();
            }

            IEnumerable<int> postIds = postSearchEngine.Search(post, pageSize + 1);

            IQueryable<Post> posts = postRepository.GetAllAvailable().Where(p => postIds.Contains(p.Id) && p.Id != id);

            SearchResult<Post> result = await SearchResult<Post>.CreateAsync(posts, page, pageSize);

            if (result.TotalCount == 0)
            {
                return new NotFoundResult();
            }

            // since we need only enough info to generate links on client side, remove body from response
            foreach (Post p in result.Data)
            {
                p.BodyStart = null;
                p.BodyEnd = null;
            }

            return result.ToDto<PostDto>(mapper);
        }

        [HttpPut("{id:int}/vote/{voteUp:bool}")]
        public async Task<ActionResult> Vote(int id, bool voteUp)
        {
            Post post = await postRepository.GetAsync(id);

            if (post == null)
            {
                return new NotFoundResult();
            }

            if (voteUp)
            {
                post.Rating.VotesUp += 1;
            }
            else
            {
                post.Rating.VotesDown += 1;
            }

            await postRepository.UpdateAsync(post);

            return Ok();
        }
        
        [HttpPost]
        [Authorize(IdentityServerConstants.LocalApi.PolicyName)]
        public async Task<ActionResult<PostDto>> Post(PostDto model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model), "Model can not be null.");
            }

            var post = mapper.Map<Post>(model);
            post.UrlSlug = urlizer.GetFriendlyUrl(post.Title);
            post.Category = await categoryRepository.GetAsync(model.Category.Id);

            foreach (TagDto tag in model.Tags)
            {
                var existingTag = await tagRepository.GetAsync(tag.Id);
                if (existingTag == null)
                {
                    existingTag = await tagRepository.InsertAsync(new Tag()
                    {
                        Title = tag.Title,
                        UrlSlug = urlizer.GetFriendlyUrl(tag.UrlSlug)
                    });
                }
                
                post.TagPosts.Add(new TagPosts { TagId = tag.Id, PostId = post.Id, Tag = existingTag, Post = post });
            }

            Post result = await postRepository.InsertAsync(post);
            
            model.Id = result.Id;

            return model;
        }

        [HttpPut("{id:int}")]
        [Authorize(IdentityServerConstants.LocalApi.PolicyName)]
        public async Task<ActionResult<PostDto>> Put(PostDto model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model), "Model can not be null.");
            }
            
            Post post = await postRepository.GetAsync(model.Id);

            if (post == null)
            {
                return new NotFoundResult();
            }

            post = mapper.Map<Post>(model);
            post.UrlSlug = urlizer.GetFriendlyUrl(post.Title);

            foreach (TagDto tag in model.Tags)
            {
                var existingTag = await tagRepository.GetAsync(tag.Id);
                if (existingTag == null)
                {
                    existingTag = await tagRepository.InsertAsync(new Tag()
                    {
                        Title = tag.Title,
                        UrlSlug = urlizer.GetFriendlyUrl(tag.UrlSlug)
                    });
                }
                
                post.TagPosts.Add(new TagPosts { TagId = tag.Id, PostId = post.Id, Tag = existingTag, Post = post });
            }

            post = await postRepository.UpdateAsync(post);

            postSearchEngine.AddPostToIndex(post);

            return model;
        }
        
        [HttpDelete("{id:int}")]
        [Authorize(IdentityServerConstants.LocalApi.PolicyName)]
        public async Task Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("ID of Post should be positive.", nameof(id));
            }

            await postRepository.DeleteAsync(id);
        }


        #endregion


        #region Methods

        private async Task<SearchResult<Post>> GetSearchResult(IQueryable<Post> posts,
                                                               int page,
                                                               int pageSize,
                                                               string sortColumn = null,
                                                               string sortOrder = null)
        {
            SearchResult<Post> result;
            if (sortColumn == "Rating")
            {
                posts = posts.OrderByDescending(p => p.Rating.VotesUp - p.Rating.VotesDown);
                result = await SearchResult<Post>.CreateAsync(posts, page, pageSize);
            }
            else
            {
                result = await SearchResult<Post>.CreateAsync(posts, page, pageSize, sortColumn, sortOrder);
            }

            return result;
        }

        #endregion
    }
}
