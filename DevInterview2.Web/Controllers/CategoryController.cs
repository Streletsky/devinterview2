﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DevInterview2.Common.Helpers;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business.Interfaces;
using DevInterview2.Web.DataTransferObjects;
using Duende.IdentityServer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DevInterview2.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        #region Fields

        private readonly ICategoryRepository categoryRepository;

        private readonly IPostRepository postRepository;

        private readonly IUrlizer urlizer;

        private readonly ILogger<CategoryController> logger;

        private readonly IMapper mapper;

        #endregion


        #region Constructors and Destructors

        public CategoryController(ILogger<CategoryController> logger,
                                  IMapper mapper,
                                  ICategoryRepository categoryRepository,
                                  IPostRepository postRepository,
                                  IUrlizer urlizer)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.categoryRepository = categoryRepository;
            this.postRepository = postRepository;
            this.urlizer = urlizer;
        }

        #endregion


        #region Public Methods and Operators

        [HttpGet]
        public async Task<ActionResult<CategoryDto[]>> GetCategories()
        {
            IOrderedQueryable<Category> categories = categoryRepository.GetAll().OrderBy(c => c.Title);

            if (!categories.Any())
            {
                return new NotFoundResult();
            }

            return await categories.ProjectTo<CategoryDto>(mapper.ConfigurationProvider).Where(t => t.PostsCount > 0)
                                   .ToArrayAsync();
        }

        [HttpGet("{id:int}/posts")]
        public async Task<ActionResult<SearchResult<PostDto>>> GetPostsByCategory(
            [FromRoute]int id,
            [FromQuery]int page = 1,
            [FromQuery]int pageSize = 10)
        {
            var posts = postRepository.GetByCategory(id);

            SearchResult<Post> result = await SearchResult<Post>.CreateAsync(posts, page, pageSize, "Created", "DESC");

            if (result.TotalCount == 0)
            {
                return new NotFoundResult();
            }

            // we don't need second half of body when we query posts list
            foreach (Post p in result.Data)
            {
                p.BodyEnd = null;
            }

            return result.ToDto<PostDto>(mapper);
        }
        
        [HttpPost]
        [Authorize(IdentityServerConstants.LocalApi.PolicyName)]
        public async Task<ActionResult<CategoryDto>> Post(CategoryDto model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model), "Model can not be null.");
            }

            var category = mapper.Map<Category>(model);
            category.UrlSlug = urlizer.GetFriendlyUrl(category.Title);

            Category result = await categoryRepository.InsertAsync(category);
            
            model.Id = result.Id;

            return model;
        }

        [HttpPut("{id:int}")]
        [Authorize(IdentityServerConstants.LocalApi.PolicyName)]
        public async Task<ActionResult<CategoryDto>> Put(CategoryDto model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model), "Model can not be null.");
            }
            
            Category category = await categoryRepository.GetAsync(model.Id);

            if (category == null)
            {
                return new NotFoundResult();
            }

            category = mapper.Map<Category>(model);
            category.UrlSlug = urlizer.GetFriendlyUrl(category.Title);

            await categoryRepository.UpdateAsync(category);

            return model;
        }
        
        [HttpDelete("{id:int}")]
        [Authorize(IdentityServerConstants.LocalApi.PolicyName)]
        public async Task Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException("ID of Post should be positive.", nameof(id));
            }

            await categoryRepository.DeleteAsync(id);
        }

        #endregion
    }
}
