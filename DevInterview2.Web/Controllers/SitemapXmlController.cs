﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business.Interfaces;
using DevInterview2.Services.SitemapGenerator;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DevInterview2.Web.Controllers
{
    public class SitemapXmlController : Controller
    {
        #region Fields

        private readonly ICategoryRepository categoryReporisory;

        private readonly ILogger<SitemapXmlController> logger;

        private readonly IPostRepository postRepository;

        private readonly ISitemapGenerator sitemapGenerator;

        private readonly ITagRepository tagRepository;

        #endregion


        #region Constructors and Destructors

        public SitemapXmlController(ILogger<SitemapXmlController> logger,
                                    ISitemapGenerator sitemapGenerator,
                                    IPostRepository postRepository,
                                    ITagRepository tagRepository,
                                    ICategoryRepository categoryReporisory)
        {
            this.logger = logger;
            this.sitemapGenerator = sitemapGenerator;
            this.postRepository = postRepository;
            this.tagRepository = tagRepository;
            this.categoryReporisory = categoryReporisory;
        }

        #endregion


        #region Public Methods and Operators

        [Route("/sitemap.xml")]
        public async Task<ActionResult> Index()
        {
            logger.LogInformation($"sitemap.xml requested by UA: {Request.Headers["UserAgent"]}, IP: {Request.Headers["UserHostAddress"]}");

            var absoluteRootUrl = $"{Request.Scheme}://{Request.Host}/";

            IEnumerable<Post> posts = await postRepository.GetAll().OrderBy(p => p.Created).ToListAsync();
            IEnumerable<Category> cats = await categoryReporisory.GetAll().ToListAsync();
            IEnumerable<Tag> tags = await tagRepository.GetAll().ToListAsync();
            DateTime lastModified = posts.Max(p => p.Created);

            sitemapGenerator.AddSitemapNode(absoluteRootUrl, 1.0, SitemapFrequency.Daily, lastModified);
            sitemapGenerator.AddSitemapNode(absoluteRootUrl + "most-viewed", 0.9, SitemapFrequency.Weekly);
            sitemapGenerator.AddSitemapNode(absoluteRootUrl + "top-rated", 0.9, SitemapFrequency.Weekly);
            sitemapGenerator.AddSitemapNode(absoluteRootUrl + "about", 0.7, SitemapFrequency.Weekly);
            sitemapGenerator.AddSitemapNode(absoluteRootUrl + "search", 0.7, SitemapFrequency.Weekly);

            foreach (Category cat in cats)
            {
                sitemapGenerator.AddSitemapNode(absoluteRootUrl + "c/" + cat.UrlSlug, 0.9, SitemapFrequency.Daily);
            }

            foreach (Tag tag in tags)
            {
                sitemapGenerator.AddSitemapNode(absoluteRootUrl + "t/" + tag.UrlSlug, 0.9, SitemapFrequency.Daily);
            }

            foreach (Post post in posts)
            {
                sitemapGenerator.AddSitemapNode($"{absoluteRootUrl}p/{post.Category.UrlSlug}/{post.UrlSlug}-{post.Id}",
                                                0.8,
                                                SitemapFrequency.Weekly,
                                                post.LastUpdated);
            }

            string xml = sitemapGenerator.BuildSitemapDocument();

            return Content(xml, MediaTypeNames.Text.Xml, Encoding.UTF8);
        }

        #endregion
    }
}