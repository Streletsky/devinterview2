﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business.Interfaces;
using DevInterview2.Web.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DevInterview2.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TagController : ControllerBase
    {
        #region Fields

        private readonly ILogger<TagController> logger;

        private readonly IMapper mapper;

        private readonly ITagRepository tagRepository;

        private readonly IPostRepository postRepository;

        #endregion


        #region Constructors and Destructors

        public TagController(ILogger<TagController> logger, IMapper mapper, ITagRepository tagRepository, IPostRepository postRepository)
        {
            this.logger = logger;
            this.tagRepository = tagRepository;
            this.postRepository = postRepository;
            this.mapper = mapper;
        }

        #endregion


        #region Public Methods and Operators

        [HttpGet("{id:int}/posts")]
        public async Task<ActionResult<SearchResult<PostDto>>> GetPostsByTag([FromRoute]int id, [FromQuery]int page = 1, [FromQuery]int pageSize = 10)
        {
            var posts = postRepository.GetByTag(id);

            SearchResult<Post> result = await SearchResult<Post>.CreateAsync(posts, page, pageSize, "Created", "DESC");

            if (result.TotalCount == 0)
            {
                return new NotFoundResult();
            }

            // we don't need second half of body when we query posts list
            foreach (Post p in result.Data)
            {
                p.BodyEnd = null;
            }

            return result.ToDto<PostDto>(mapper);
        }

        [HttpGet]
        public async Task<ActionResult<TagDto[]>> GetTags()
        {
            IQueryable<Tag> tags = tagRepository.GetAllAvailable();

            if (!tags.Any())
            {
                return new NotFoundResult();
            }

            return await tags.ProjectTo<TagDto>(mapper.ConfigurationProvider).OrderByDescending(c => c.PostsCount)
                             .ToArrayAsync();
        }

        #endregion
    }
}
