﻿using System.Collections.Generic;
using System.Linq;
using DevInterview2.Data.Entities.Business;
using DevInterview2.Data.Repositories.Business.Interfaces;
using DevInterview2.Services.SearchEngine;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DevInterview2.Web.Infrastructure
{
    public static class HostExtensions
    {
        #region Public Methods and Operators

        public static IHost RefreshLuceneIndex(this IHost webHost)
        {
            using var scope = webHost.Services.CreateScope();
            var postRepository = scope.ServiceProvider.GetService<IPostRepository>();
            var searchEngine = scope.ServiceProvider.GetService<IPostSearchEngine>();
            List<Post> posts = postRepository.GetAllAvailable().ToList();

            searchEngine.AddPostsToIndex(posts);

            return webHost;
        }

        #endregion
    }
}
