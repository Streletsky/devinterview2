using DevInterview2.Common.Helpers;
using DevInterview2.Data.Entities.Identity;
using DevInterview2.Data.OrmInfrastructure;
using DevInterview2.Data.Repositories.Business;
using DevInterview2.Data.Repositories.Business.Interfaces;
using DevInterview2.Services.SearchEngine;
using DevInterview2.Services.SitemapGenerator;
using DevInterview2.Web.Infrastructure;
using Duende.IdentityServer;
using Duende.IdentityServer.Models;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");

builder.Services.AddDbContext<AppDbContext>(options =>
                                                        options.UseSqlServer(connectionString));

builder.Services.AddDatabaseDeveloperPageExceptionFilter();
builder.Services.AddApplicationInsightsTelemetry();

builder.Services.AddDefaultIdentity<AppUser>(options => options.SignIn.RequireConfirmedAccount = true)
       .AddEntityFrameworkStores<AppDbContext>();

builder.Services.AddIdentityServer().AddApiAuthorization<AppUser, AppDbContext>(options =>
{
    options.ApiScopes.Add(new ApiScope(IdentityServerConstants.LocalApi.ScopeName));
    options.Clients.First().AllowedScopes.Add(IdentityServerConstants.LocalApi.ScopeName);
});
builder.Services.AddAuthentication().AddIdentityServerJwt();
builder.Services.AddLocalApiAuthentication();
builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();
builder.Services.AddControllers().AddApplicationPart(typeof(Program).Assembly);

/*builder.Services.Configure<JwtBearerOptions>(
                                             IdentityServerJwtConstants.IdentityServerJwtBearerScheme, 
                                             options =>
                                             {
                                                 options.Authority = "https://developer-interview.com";
                                             });*/


builder.Services.AddAutoMapper(typeof(Program).Assembly);

builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
});

builder.Services.AddSingleton<IPostSearchEngine>(new LucenePostSearchEngine(builder.Environment.ContentRootPath));
builder.Services.AddScoped<ISitemapGenerator, SitemapGenerator>();
builder.Services.AddScoped<IBotDetector, BotDetector>();
builder.Services.AddScoped<IPostRepository, PostRepository>();
builder.Services.AddScoped<ICategoryRepository, CategoryRepository>();
builder.Services.AddScoped<ITagRepository, TagRepository>();
builder.Services.AddScoped<IUrlizer, Urlizer>();

builder.Logging.ClearProviders();
builder.Logging.AddConsole();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Error");
    
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseApplicationInsightsRequestTelemetry();
app.UseApplicationInsightsExceptionTelemetry();

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseCors("CorsPolicy");

app.UseAuthentication();
app.UseRouting();
app.UseIdentityServer();
app.UseAuthorization();

app.MapControllerRoute("sitemap",
                       "sitemap.xml",
                       new
                       {
                           controller = "SitemapXml",
                           action = "Index"
                       });

app.MapControllerRoute(
                       name: "default",
                       pattern: "{controller}/{action=Index}/{id?}");
app.MapRazorPages();

app.MapFallbackToFile("index.html");

app.RefreshLuceneIndex();

app.Run();
