﻿using AutoMapper;
using DevInterview2.Data.Entities.Business;

namespace DevInterview2.Web.DataTransferObjects
{
    public class RatingDto
    {
        #region Public Properties

        public int Id { get; set; }

        public int VotesDifference { get; set; }

        public int VotesDown { get; set; }

        public int VotesUp { get; set; }

        #endregion
    }

    public class RatingDtoProfile : Profile
    {
        #region Constructors and Destructors

        public RatingDtoProfile()
        {
            CreateMap<Rating, RatingDto>();
            CreateMap<RatingDto, Rating>();
        }

        #endregion
    }
}