﻿using AutoMapper;
using DevInterview2.Data.Entities.Business;

namespace DevInterview2.Web.DataTransferObjects
{
    public class TagDto
    {
        #region Public Properties

        public int Id { get; set; }

        public int PostsCount { get; set; }

        public string Title { get; set; }

        public string UrlSlug { get; set; }

        #endregion
    }

    public class TagDtoProfile : Profile
    {
        #region Constructors and Destructors

        public TagDtoProfile()
        {
            CreateMap<Tag, TagDto>().ForMember(t => t.PostsCount, opt => opt.MapFrom(tag => tag.TagPosts.Count));
            CreateMap<TagDto, Tag>().ForMember(t => t.TagPosts, opt => opt.Ignore());
        }

        #endregion
    }
}