﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DevInterview2.Data.Entities.Business;

namespace DevInterview2.Web.DataTransferObjects
{
    public class PostDto
    {
        #region Public Properties

        public string BodyEnd { get; set; }

        public string BodyStart { get; set; }

        public CategoryDto Category { get; set; }

        public DateTime Created { get; set; }

        public string Description { get; set; }

        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsLinkingRequired { get; set; }

        public bool IsVisible { get; set; }

        public DateTime LastUpdated { get; set; }

        public RatingDto Rating { get; set; }

        public virtual ICollection<TagDto> Tags { get; set; }

        public string Title { get; set; }

        public string UrlSlug { get; set; }

        public int Views { get; set; }

        #endregion
    }

    public class PostDtoProfile : Profile
    {
        #region Constructors and Destructors

        public PostDtoProfile()
        {
            CreateMap<Post, PostDto>()
                .ForMember(c => c.Tags, opt => opt.MapFrom(t => t.TagPosts.Select(tp => tp.Tag)));
            CreateMap<PostDto, Post>().ForMember(p => p.CreatedBy, opt => opt.Ignore())
                                      .ForMember(p => p.LastUpdatedBy, opt => opt.Ignore())
                                      .ForMember(p => p.CreatedById, opt => opt.Ignore())
                                      .ForMember(p => p.LastUpdatedById, opt => opt.Ignore())
                                      .ForMember(p => p.TagPosts, opt => opt.Ignore());
        }

        #endregion
    }
}
