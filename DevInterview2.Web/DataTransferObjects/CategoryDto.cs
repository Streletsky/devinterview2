﻿using AutoMapper;
using DevInterview2.Data.Entities.Business;

namespace DevInterview2.Web.DataTransferObjects
{
    public class CategoryDto
    {
        #region Public Properties

        public string Description { get; set; }

        public int Id { get; set; }

        public int PostsCount { get; set; }

        public string Title { get; set; }

        public string UrlSlug { get; set; }

        #endregion
    }

    public class CategoryDtoProfile : Profile
    {
        #region Constructors and Destructors

        public CategoryDtoProfile()
        {
            CreateMap<Category, CategoryDto>()
                .ForMember(c => c.PostsCount, opt => opt.MapFrom(category => category.Posts.Count));
            CreateMap<CategoryDto, Category>().ForMember(c => c.Posts, opt => opt.Ignore());
        }

        #endregion
    }
}