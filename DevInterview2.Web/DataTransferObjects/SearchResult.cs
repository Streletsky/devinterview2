﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using DevInterview2.Common.Exceptions.Business;
using DevInterview2.Common.Extensions;
using Microsoft.EntityFrameworkCore;

namespace DevInterview2.Web.DataTransferObjects
{
    /// <summary>
    ///     Wrapper for results of search query.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SearchResult<T>
    {
        #region Constructors and Destructors

        private SearchResult(IEnumerable<T> data,
                             int totalCount,
                             int page,
                             int pageSize,
                             string sortColumn,
                             string sortOrder)
        {
            Data = data;
            TotalCount = totalCount;
            Page = page;
            PageSize = pageSize;
            SortColumn = sortColumn;
            SortOrder = sortOrder;
            TotalPages = (int)Math.Ceiling(totalCount / (double)PageSize);
        }

        #endregion


        #region Public Properties

        public IEnumerable<T> Data { get; }

        public int Page { get; }

        public int PageSize { get; }

        public string SortColumn { get; }

        public string SortOrder { get; }

        public int TotalCount { get; }

        public int TotalPages { get; }

        #endregion


        #region Public Methods and Operators

        public static SearchResult<T> Create(IEnumerable<T> source,
                                             int page,
                                             int pageSize)
        {
            int records = source.Count();

            source = source.Skip((page - 1) * pageSize).Take(pageSize);

            List<T> data = source.ToList();

            return new SearchResult<T>(data, records, page, pageSize, null, null);
        }

        public static async Task<SearchResult<T>> CreateAsync(IQueryable<T> source,
                                                              int page,
                                                              int pageSize,
                                                              string sortColumn = null,
                                                              string sortOrder = null)
        {
            int records = await source.CountAsync();

            if (!sortColumn.IsNullOrEmpty() && IsValidProperty(sortColumn))
            {
                sortOrder = sortOrder?.ToUpper() == "ASC" ? "ASC" : "DESC";

                source = source.OrderBy($"{sortColumn} {sortOrder}");
            }

            source = source.Skip((page - 1) * pageSize).Take(pageSize);

            List<T> data = await source.ToListAsync();

            return new SearchResult<T>(data, records, page, pageSize, sortColumn, sortOrder);
        }

        public SearchResult<TDto> ToDto<TDto>(IMapper mapper) =>
            new SearchResult<TDto>(mapper.ProjectTo<TDto>(Data.AsQueryable()),
                                   TotalCount,
                                   Page,
                                   PageSize,
                                   SortColumn,
                                   SortOrder);

        #endregion


        #region Methods

        private static bool IsValidProperty(string propertyName, bool throwExceptionIfNotValid = true)
        {
            PropertyInfo prop = typeof(T).GetProperty(propertyName,
                                                      BindingFlags.IgnoreCase |
                                                      BindingFlags.Public |
                                                      BindingFlags.Instance);

            if (prop == null && throwExceptionIfNotValid)
            {
                throw new ValidationException($"Property {propertyName} does not exist.");
            }

            return prop != null;
        }

        #endregion
    }
}
